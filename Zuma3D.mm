<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Zuma3D" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1361701815431"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7"/>
<node TEXT="Map" POSITION="right" ID="ID_749359931" CREATED="1361703823815" MODIFIED="1361703854607">
<edge COLOR="#ff0000"/>
<node TEXT="Parametric path for incoming balls" ID="ID_121478479" CREATED="1361703859375" MODIFIED="1361703866968"/>
<node TEXT="Free third person movement" ID="ID_1326116255" CREATED="1361703868510" MODIFIED="1361703885650">
<node TEXT="Vertical angle?" ID="ID_687635030" CREATED="1361703888667" MODIFIED="1361703894804"/>
<node TEXT="Physics?" ID="ID_1252708714" CREATED="1361703895360" MODIFIED="1361703900168"/>
</node>
<node TEXT="Tunnels/not whole path visible" ID="ID_1489048279" CREATED="1361704214646" MODIFIED="1361704224945"/>
</node>
<node TEXT="Techniques" POSITION="left" ID="ID_1064798724" CREATED="1361703907787" MODIFIED="1361703910357">
<edge COLOR="#ff00ff"/>
<node TEXT="Spherical Harmonics for every ball" ID="ID_1487876148" CREATED="1361704189312" MODIFIED="1361704199101"/>
<node TEXT="Depth of view" ID="ID_1206140261" CREATED="1361704229711" MODIFIED="1361704630527"/>
<node TEXT="" ID="ID_593034885" CREATED="1361704631234" MODIFIED="1361704631234"/>
</node>
<node TEXT="Interface" POSITION="right" ID="ID_664929954" CREATED="1361705757760" MODIFIED="1361705761875">
<edge COLOR="#00ffff"/>
<node TEXT="Main Menu" ID="ID_1971503638" CREATED="1361705763084" MODIFIED="1361705765239"/>
<node TEXT="third person" ID="ID_1238742979" CREATED="1361705765698" MODIFIED="1361705804024">
<node TEXT="camera for looking around" ID="ID_17806421" CREATED="1361705789880" MODIFIED="1361705796430"/>
<node TEXT="WASD controls" ID="ID_118300327" CREATED="1361705805722" MODIFIED="1361705808698"/>
</node>
<node TEXT="Top-down" ID="ID_1277640038" CREATED="1361705810301" MODIFIED="1361705813536">
<node TEXT="WASD controls" ID="ID_1490449162" CREATED="1361705814543" MODIFIED="1361705823873"/>
<node TEXT="Mous controls" ID="ID_1529022928" CREATED="1361705824206" MODIFIED="1361705827358"/>
</node>
</node>
<node TEXT="Gameplay" POSITION="left" ID="ID_910856048" CREATED="1361705837942" MODIFIED="1361705840446">
<edge COLOR="#ffff00"/>
<node TEXT="" ID="ID_1730521503" CREATED="1361705846818" MODIFIED="1361705846818"/>
</node>
</node>
</map>
