#version 150 core

struct Light
{
	vec3 position;
	int type;
	float intensity;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

uniform sampler2D diffuse_texture;

uniform vec3 material_diffuse_color;
uniform vec3 material_specular_color;
uniform vec3 material_ambient_color;
uniform vec3 material_emissive_color;
uniform float material_transparency;
uniform float material_shininess;

uniform int material_diffuse_texture;

uniform vec3 viewPosition;

in vec3 pass_Normal;
in vec3 pass_Position;
in vec3 pass_UV;
out vec4 out_Color;

uniform int lightCount;
// maximum 10 lights per scene
uniform Light lights[10];

vec3 calculateSpecular(vec3 specularLight, vec3 materialSpecular, float materialShininess, vec3 normal, vec3 directionToLight, vec3 directionFromEye)
{
	float normalizationFactor = ((materialShininess + 2.0) / 8.0);
	vec3 h = normalize(directionToLight + directionFromEye);
	return normalizationFactor * pow(max(0,dot(h, normal)), materialShininess)*materialSpecular*specularLight;
	
}

void main(void) {

	vec3 final_color = vec3(0,0,0);	
	vec3 ambient_component = vec3(0,0,0);
	vec3 diffuse_component = vec3(0,0,0);
	vec3 specular_component = vec3(0,0,0);

	vec3 normal = normalize(pass_Normal);
	vec3 frag_pos = pass_Position;
	vec3 dirToView = normalize(viewPosition-frag_pos);
	
	vec3 light_pos;
	vec3 dirToLight;
	float received_intensity;
	float shade;
	for(int i=0;i<lightCount;++i) {
		light_pos = lights[i].position;
		dirToLight = normalize(light_pos-frag_pos);
		received_intensity = (lights[i].intensity - length(frag_pos-light_pos)) / lights[i].intensity;
		shade = dot(dirToLight,normal);
		
		ambient_component += material_ambient_color*lights[i].ambient;	
		final_color += ambient_component;
		// TODO fix this shading optimization
		if (shade > 0 && received_intensity > 0 ) {
			diffuse_component += material_diffuse_color*(material_diffuse_texture == 1 ? texture(diffuse_texture, pass_UV.xy).xyz : vec3(1))*lights[i].diffuse*shade;
			specular_component += calculateSpecular(lights[i].specular, material_specular_color, material_shininess, normal, dirToLight, dirToView);
			final_color += (diffuse_component+specular_component+material_emissive_color)*received_intensity;
		}
	}
	out_Color = vec4(final_color,material_transparency);
}

