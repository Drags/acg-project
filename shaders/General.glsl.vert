#version 150 core

layout(location=0) in vec3 in_Position;
layout(location=1) in vec3 in_UV;
layout(location=2) in vec3 in_Normal;

uniform mat4 projectionViewModelMatrix;
uniform mat4 normalMatrix;
uniform mat4 modelMatrix;

out vec3 pass_Normal;
out vec3 pass_UV;
out vec3 pass_Position;

void main(void) {
	pass_Position = (modelMatrix*vec4(in_Position, 1)).xyz;
	gl_Position = projectionViewModelMatrix*vec4(in_Position,1);
	//pass_Normal = in_Normal;
	pass_Normal = (normalMatrix*vec4(in_Normal, 0)).xyz;
	pass_UV = in_UV;
}