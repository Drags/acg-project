#version 150 core

layout(location=0) in vec3 in_Position;
layout(location=1) in vec3 in_UV;
layout(location=3) in vec3 SH[4];

uniform mat4 projectionViewModelMatrix;
uniform mat4 modelMatrix;

struct SHLight {
	vec3 position;
	float intensity;
	vec3 coefs[4];
};

uniform SHLight lights[10];
uniform int totalLights;

out vec3 pass_Color;
out vec3 pass_UV;

out vec3 fragPosition;

void main(void) {
	gl_Position = projectionViewModelMatrix*vec4(in_Position,1);
	pass_Color = vec3(0);
	
	vec3 ws_Position = (modelMatrix*vec4(in_Position,1)).xyz;
	fragPosition = ws_Position;
	
	for (int j = 0; j < totalLights; ++j) {
		vec3 lightContribution = vec3(0);
		for (int i =0; i < 4; ++i) {
			lightContribution += SH[i]*lights[j].coefs[i];
		}
		pass_Color += lightContribution*clamp((1.0 - length(lights[j].position-ws_Position)/lights[j].intensity),0.0,1.0);
	}
	pass_UV = in_UV;
}