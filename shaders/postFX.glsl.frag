#version 150 core

uniform float healthPercent;

uniform vec2 lightPosition = vec2(0.5,0.5);

uniform int NUM_SAMPLES = 30;
uniform float density = 0.2;

uniform float Decay = 0.95;
uniform float Weight = 0.05;

uniform sampler2D fbo_texture;
varying vec2 f_texcoord;

 
void main(void) {
	vec2 realLight = lightPosition;
	vec2 textureCoord = vec2(f_texcoord);
	vec2 deltaTexCoord = (textureCoord - realLight);
	float blendFactor =  clamp(length(deltaTexCoord),0,1);
	deltaTexCoord *= 1.0 / NUM_SAMPLES * density;
	vec3 color = texture2D(fbo_texture, textureCoord).xyz;
	float illuminationDecay = 1.0;

	for (int i = 0; i < NUM_SAMPLES; i++) {
		textureCoord -= deltaTexCoord;
		vec3 sample = texture2D(fbo_texture, textureCoord).xyz;
		sample *= illuminationDecay * Weight;
		color += sample;
		illuminationDecay *= Decay;
	}
	if (gl_FragCoord.x > 30 && gl_FragCoord.x < 130 && gl_FragCoord.y > 30 && gl_FragCoord.y < 50) {
		vec3 hpColor = (gl_FragCoord.x > 30+100*healthPercent ? vec3(0,0,0):vec3(1,0,0));
		gl_FragColor = vec4(hpColor,1);
	} else {
		gl_FragColor = vec4(color*(1.0-blendFactor)+(blendFactor)*texture2D(fbo_texture, f_texcoord),1);
	}
}