#version 150 core

uniform sampler2D diffuse_texture;
uniform int material_diffuse_texture;
uniform vec3 viewPosition;

uniform vec3 fogColor;
uniform float density;

in vec3 fragPosition;

in vec3 pass_UV;
in vec3 pass_Color;
out vec4 out_Color;

const float LOG2 = 1.442695;
void main(void) {
	float dist = length(viewPosition-fragPosition);
	float fogFactor = exp2( -density * 
					   density * 
					   dist * 
					   dist * 
					   LOG2 );
	fogFactor = clamp(fogFactor, 0.0, 1.0);
	vec3 texture = material_diffuse_texture == 1 ? texture(diffuse_texture, pass_UV.xy).xyz : vec3(1);
	vec3 cmp = texture;
	out_Color = vec4(mix(fogColor, cmp, fogFactor), 1);
}

