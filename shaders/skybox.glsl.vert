#version 150

layout(location=0) in vec3 in_Position;
layout(location=1) in vec3 in_UV;

uniform mat4 projectionViewModelMatrix;
uniform mat4 modelMatrix;

out vec3 pass_UV;
out vec3 pass_Color;

out vec3 fragPosition;

void main(void) {
	gl_Position = projectionViewModelMatrix*vec4(in_Position,1);
	pass_Color = vec3(0);
	
	vec3 ws_Position = (modelMatrix*vec4(in_Position,1)).xyz;
	fragPosition = ws_Position;
	pass_UV = in_UV;
}
