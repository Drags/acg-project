package com.elecfant.acg;

import java.io.File;
import java.io.IOException;

import com.elecfant.acg.obj.OBJModel;
import com.elecfant.lib.Map;

public class AssetManager {

	private static boolean isInitialized = false;

	private static Map<String, OBJModel> OBJModels;

	public static void Initialize() {
		OBJModels = new Map<String, OBJModel>();
		isInitialized = true;
	}

	public static OBJModel get(String filename) {
		if (!isInitialized) {
			outputMessage("Have not been initialized. Call AssetManager.Initialize() first");
			return null;
		}
		final File f = new File(filename);
		try {
			String absoluteFilename = f.getCanonicalPath();
			if (OBJModels.containsKey(absoluteFilename)) {
				return OBJModels.get(absoluteFilename);
			}
			OBJModel loadedModel = new OBJModel(f);
			OBJModels.put(absoluteFilename, loadedModel);
			return loadedModel;
		} catch (IOException e) {
			outputMessage("Can NOT load model '" + filename + "'.");
		}
		return null;
	}
	
	public static void Cleanup() {
		for (OBJModel object : OBJModels) {
			object.destroy();
		}
	}

	private static void outputMessage(String message) {
		System.out.println("<AssetManager> " + message);
	}

	public static void preload(String string) {
		get(string);
	}
}
