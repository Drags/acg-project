package com.elecfant.acg;

import static com.elecfant.acg.utils.Utils.check_gl_error;
import static org.lwjgl.opengl.GL11.GL_BACK;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FILL;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_LINE;
import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_POINT;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glPolygonMode;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT16;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.GL_COLOR_ATTACHMENT0;
import static org.lwjgl.opengl.GL30.GL_DEPTH_ATTACHMENT;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.GL_RENDERBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;
import static org.lwjgl.opengl.GL30.glBindRenderbuffer;
import static org.lwjgl.opengl.GL30.glDeleteFramebuffers;
import static org.lwjgl.opengl.GL30.glDeleteRenderbuffers;
import static org.lwjgl.opengl.GL30.glFramebufferRenderbuffer;
import static org.lwjgl.opengl.GL30.glFramebufferTexture2D;
import static org.lwjgl.opengl.GL30.glGenFramebuffers;
import static org.lwjgl.opengl.GL30.glGenRenderbuffers;
import static org.lwjgl.opengl.GL30.glRenderbufferStorage;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.ReadableVector3f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import com.elecfant.acg.game.AITankEntity;
import com.elecfant.acg.game.Camera;
import com.elecfant.acg.game.GameEntity;
import com.elecfant.acg.game.Light;
import com.elecfant.acg.game.ModelEntity;
import com.elecfant.acg.game.PlayerTankEntity;
import com.elecfant.acg.game.SpotLight;
import com.elecfant.acg.game.TankEntity;
import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.obj.OBJModel;
import com.elecfant.acg.utils.MathUtils;
import com.elecfant.acg.utils.ShaderProgram;

public class MainACG {

	private static final ReadableVector3f DEFAULT_FOLLOW_DISTANCE = new Vector3f(
			0, 10, 20);

	private long lastTime = getMiliseconds();

	static public ArrayList<GameEntity> entities;
	static public ArrayList<GameEntity> justCreatedEntities;
	static public ArrayList<GameEntity> toBeDestroyed;

	// TODO create settings files
	private static boolean fullScreen = false;
	private static int width = 640;
	private static int height = 480;
	private DisplayMode currentDisplayMode;

	private Camera camera;

	private Light defaultLight;

	private ShaderProgram[] shaderSHProgram;
	private ShaderProgram shaderGeneralProgram;
	private ShaderProgram postFXShader;

	private boolean shouldQuit = false;

	private ArrayList<Light> lights;

	private GameEntity player;

	private boolean useSH = true;

	private float keyDownTime;

	private float fogDensity = 0.002f;

	private boolean useVSync = false;

	private Vector3f backgroundColor = new Vector3f(0.058823529f, 0.03921568f, 0.0196f);

	private int fbo_texture = -1;
	private int rbo_depth = -1;
	private int fbo = -1;

	private int vbo_fbo_vertices = -1;

	private ModelEntity skybox;

	private ShaderProgram skyBoxShader;

	private int SOL_NUM_SAMPLES = 62;
	private float SOL_density = 0.65354097f;
	private float SOL_Decay = 0.95f;
	private float SOL_Weight = 0.060405433f;

	private boolean debug = false;

	private boolean demo = false;
	
	private long getMiliseconds() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	private void Initialize(String[] argv) {

		// processing command line parameters
		for (int i = 0; i < argv.length; ++i) {
			if (argv[i].equals("-fs")) {
				fullScreen = true;
				continue;
			}
			if (argv[i].equals("-w")) {
				width = Integer.parseInt(argv[++i]);
				continue;
			}
			if (argv[i].equals("-h")) {
				height = Integer.parseInt(argv[++i]);
				continue;
			}
			if (argv[i].equals("-d")) {
				// setup all the debug variables
				OBJModel.debug = true;
				this.debug = true;
				continue;
			}
			if (argv[i].equals("+vs")) {
				useVSync = true;
			}
			if (argv[i].equals("-vs")) {
				useVSync = false;
			}
			if (argv[i].equals("demo")) {
				demo = true;
			}
		}

		try {

			PixelFormat pixelFormat = new PixelFormat();
			ContextAttribs contextAtrributes = new ContextAttribs(4, 2)
					.withForwardCompatible(true).withProfileCore(true);
			for (DisplayMode dm : Display.getAvailableDisplayModes()) {
				if ((dm.isFullscreenCapable() || !fullScreen)
						&& (dm.getWidth() == width && dm.getHeight() == height)) {
					currentDisplayMode = dm;
				}
			}
			if (currentDisplayMode == null) {
				currentDisplayMode = Display.getDisplayMode();
			}
			if (currentDisplayMode.isFullscreenCapable() && fullScreen) {
				System.out.println("going fullscreen");
				Display.setDisplayModeAndFullscreen(currentDisplayMode);
				// Mouse.setGrabbed(true);
			} else {
				// Mouse.setGrabbed(true);
				Display.setDisplayMode(currentDisplayMode);
			}
			Display.create(pixelFormat, contextAtrributes);
			Display.setVSyncEnabled(useVSync);
			glViewport(0, 0, width, height);
			glClearColor(backgroundColor.x, backgroundColor.y,
					backgroundColor.z, 0.0f);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glEnable(GL_DEPTH_TEST);
			check_gl_error("configuring context");
			shaderSHProgram = new ShaderProgram[5];
			for (int i = 0; i < 5; ++i) {
				shaderSHProgram[i] = new ShaderProgram("shaders/SH/SH" + i
						+ ".glsl.vert", "shaders/SH/SH.glsl.frag");
			}
			shaderGeneralProgram = new ShaderProgram(
					"shaders/General.glsl.vert", "shaders/General.glsl.frag");
			postFXShader = new ShaderProgram("shaders/postFX.glsl.vert",
					"shaders/postFX.glsl.frag");

			skyBoxShader = new ShaderProgram("shaders/skybox.glsl.vert", "shaders/skybox.glsl.frag");

			/* init_resources */

			/* Create back-buffer, used for post-processing */
			// TODO make this nice.

			/* Texture */
			glActiveTexture(GL_TEXTURE0);
			fbo_texture = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, fbo_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
					GL_UNSIGNED_BYTE,
					BufferUtils.createByteBuffer(width * height * 4));
			glBindTexture(GL_TEXTURE_2D, 0);

			/* Depth buffer */
			rbo_depth = glGenRenderbuffers();
			glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width,
					height);
			glBindRenderbuffer(GL_RENDERBUFFER, 0);

			/* Framebuffer to link everything together */
			fbo = glGenFramebuffers();
			glBindFramebuffer(GL_FRAMEBUFFER, fbo);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
					GL_TEXTURE_2D, fbo_texture, 0);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
					GL_RENDERBUFFER, rbo_depth);
			check_gl_error("creating fbo for offscreen rendering");
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			final FloatBuffer fbo_vertices = BufferUtils.createFloatBuffer(8);

			fbo_vertices.put(-1f);
			fbo_vertices.put(-1f);

			fbo_vertices.put(1f);
			fbo_vertices.put(-1f);

			fbo_vertices.put(-1f);
			fbo_vertices.put(1f);

			fbo_vertices.put(1f);
			fbo_vertices.put(1f);

			fbo_vertices.flip();

			vbo_fbo_vertices = glGenBuffers();
			glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
			glBufferData(GL_ARRAY_BUFFER, fbo_vertices, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			//

		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// -- setting up SH environment
		final int sample_q = 30;
		final int bandLimit = 4;
		SphericalHarmonic.Initialize(sample_q, bandLimit);
		SphericalHarmonic.useNumberOfBands(4);
		// --

		// -- initializing asset manager and preloading;
		AssetManager.Initialize();
		AssetManager.preload("obj/[prepared]/shell.obj");
		// --

		// -- loading test models

		toBeDestroyed = new ArrayList<GameEntity>();
		justCreatedEntities = new ArrayList<GameEntity>();
		entities = new ArrayList<GameEntity>();

		final TankEntity tank = new PlayerTankEntity(200, new Vector3f(0.075f,
				0.15f, 0.075f));
		tank.setPosition(new Vector3f(0, 100, 100));
		tank.enablePhysics();
		entities.add(tank);

		final AITankEntity enemyTank = new AITankEntity(200, new Vector3f(0.15f,
				0.15f, 0.075f));
		enemyTank.setPosition(new Vector3f(0, 100, -100));
		enemyTank.yaw((float) Math.PI);
		enemyTank.enablePhysics();
		enemyTank.setTarget(tank);
		entities.add(enemyTank);

		player = tank;

		GameEntity myEntity = new ModelEntity(
				AssetManager.get("obj/[prepared]/arena.obj"));
		myEntity.setMultiplyColor(new Vector3f(0.5f, 0.5f, 0.5f));
		entities.add(myEntity);

		skybox = new ModelEntity(AssetManager.get("obj/[prepared]/skybox.obj"));
		skybox.setPosition(new Vector3f(0, 0, 0));
		skybox.setScale(1000);

		// --

		// -- setting up camera object;
		camera = new Camera(new Vector3f(0, 25, 0), new Vector3f(
				-(float) (Math.PI / 2), 0, 0));
		camera.setPerspectiveProjection(width, height, 50.0f, 1f, 1000.0f);
		// camera.follow(player, new Vector3f(DEFAULT_FOLLOW_DISTANCE));
		camera.follow(tank, new Vector3f(DEFAULT_FOLLOW_DISTANCE));
		// camera.setAimAtTarget(true);
		// --

		// -- creating list of lights
		lights = new ArrayList<Light>();
		// ambient global light
		// defaultLight = new AmbientLight(new Vector3f(1,1,1));
		defaultLight = new SpotLight(new Vector3f(0.04f, 0.04f, 0.04f), 0.0f);
		defaultLight.setScale(2000);
		defaultLight.setPosition(new Vector3f(0, 1000, 0));
		lights.add(defaultLight);

		// defaultLight = new SpotLight((Vector3f) new Vector3f(0.02f, 0.01f,
		// 0.01f).scale(10),
		// 0.0f);
		// defaultLight.setScale(100);
		// defaultLight.setPosition(new Vector3f(200, 0, 200));
		// lights.add(defaultLight);
		//
		// defaultLight = new SpotLight((Vector3f) new Vector3f(0.01f, 0.01f,
		// 0.02f).scale(10),
		// 0.0f);
		// defaultLight.setScale(100);
		// defaultLight.setPosition(new Vector3f(-200, 0, 200));
		// lights.add(defaultLight);
		//
		// defaultLight = new SpotLight((Vector3f) new Vector3f(0.02f, 0.01f,
		// 0.02f).scale(10),
		// 0.0f);
		// defaultLight.setScale(100);
		// defaultLight.setPosition(new Vector3f(-200, 0, -200));
		// lights.add(defaultLight);
		//
		// defaultLight = new SpotLight((Vector3f) new Vector3f(0.01f, 0.02f,
		// 0.01f).scale(10),
		// 0.0f);
		// defaultLight.setScale(100);
		// defaultLight.setPosition(new Vector3f(200, 0, -200));
		// lights.add(defaultLight);

		final Light tankLight = new SpotLight(
				new Vector3f(0.02f, 0.04f, 0.02f), 0.5f) {

			// TODO create separate light class
			@Override
			public Vector3f diffuse(float x, float y, float z) {
				if (y < 0.5 && y > -0.5) {
					return this.lightColor;
				} else {
					return BLACK_COLOR;
				}
			}
		};
		tankLight.setScale(50);
		tankLight.attatchTo(tank);
		lights.add(tankLight);

		final Light enemyLight = new SpotLight(
				new Vector3f(0.04f, 0.04f, 0.02f), 0.5f) {

			// TODO create separate light class
			@Override
			public Vector3f diffuse(float x, float y, float z) {
				if (y < 0.3 && y > -0.3) {
					return this.lightColor;
				} else {
					return BLACK_COLOR;
				}
			}
		};
		enemyLight.setScale(50);
		enemyLight.attatchTo(enemyTank);
		lights.add(enemyLight);

		// --

	}

	private void Update(float deltaTime) {

		float dx = 0;
		float dy = 0;
		float dr = 0;
		while (Mouse.next()) {
			dx += Mouse.getEventDX();
			dy += Mouse.getEventDY();
			dr += Mouse.getEventDWheel();
		}
		if (dx != 0 || dy != 0 || dr != 0) {
			// TODO move in-game object, while camera is attached to it
			float rotation_speed = 3; // TODO test this out
			 player.yaw(-dx / 100.0f * rotation_speed);
			 player.pitch(dy / 100.0f * rotation_speed);
			 System.out.println(player.getRotation().y + "x" +
			 player.getRotation().x);
			if (camera.getFollowingDistance() != null) {
				final Vector3f currFollowingDistance = new Vector3f(
						camera.getFollowingDistance());
				final float delta = dr / 120;
				currFollowingDistance.z += delta;
				camera.setFollowingDistance(currFollowingDistance);
			}
		}
		if (this.debug) {
			check_gl_error("before polygon mode");
			if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_Z)) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
			}
			check_gl_error("setting polygon mode");
			if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)
					|| Display.isCloseRequested()) {
				shouldQuit = true;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
				useSH = true;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_X)) {
				useSH = false;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_1) && !Keyboard.isRepeatEvent()
					&& keyDownTime > 0.25) {
				SphericalHarmonic.updateUsedBands(1);
				System.out
						.println("no of bands: " + SphericalHarmonic.bandsInUse
								+ " number of coefs: "
								+ SphericalHarmonic.coeficientsInUse);
				keyDownTime = 0;
			} else if (Keyboard.isKeyDown(Keyboard.KEY_2)
					&& !Keyboard.isRepeatEvent() && keyDownTime > 0.25) {
				SphericalHarmonic.updateUsedBands(-1);
				System.out
						.println("no of bands: " + SphericalHarmonic.bandsInUse
								+ " number of coefs: "
								+ SphericalHarmonic.coeficientsInUse);
				keyDownTime = 0;
			} else {
				keyDownTime += deltaTime;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD4)) {
				++SOL_NUM_SAMPLES;
				System.out.println("NUM_SAMPLES: " + SOL_NUM_SAMPLES);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD1)) {
				--SOL_NUM_SAMPLES;
				System.out.println("NUM_SAMPLES: " + SOL_NUM_SAMPLES);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD5)) {
				SOL_density *= 1.01f;
				System.out.println("density: " + SOL_density);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD2)) {
				SOL_density /= 1.01f;
				System.out.println("density: " + SOL_density);
			}

			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD7)) {
				SOL_Weight *= 1.01f;
				System.out.println("Weight: " + SOL_Weight);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD8)) {
				SOL_Weight /= 1.01f;
				System.out.println("Weight: " + SOL_Weight);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD6)) {
				SOL_Decay *= 1.01f;
				System.out.println("Decay: " + SOL_Decay);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_NUMPAD3)) {
				SOL_Decay /= 1.01f;
				System.out.println("Decay: " + SOL_Decay);
			}

			if (Keyboard.isKeyDown(Keyboard.KEY_K)) {
				fogDensity *= 1.001;
				System.out.println(fogDensity);
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_L)) {
				fogDensity /= 1.001;
				System.out.println(fogDensity);
			}

			// light rotation input
			// if (Keyboard.isKeyDown(Keyboard.KEY_B)) {
			// defaultLight.rotate((float) Math.PI / 2 * deltaTime, 0, 0);
			// }
			//
			// if (Keyboard.isKeyDown(Keyboard.KEY_N)) {
			// defaultLight.rotate(0, (float) Math.PI / 2 * deltaTime, 0);
			// }
			//
			// if (Keyboard.isKeyDown(Keyboard.KEY_M)) {
			// defaultLight.rotate(0, 0, (float) Math.PI / 2 * deltaTime);
			// }

			if (Keyboard.isKeyDown(Keyboard.KEY_F)) {
				camera.follow(player, new Vector3f(DEFAULT_FOLLOW_DISTANCE));
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_G)) {
				camera.unfollow();
				camera.setPosition(new Vector3f(0, 250, 0));
				camera.setRotation(new Vector3f((float) (-Math.PI / 2), 0, 0));
			}
		}
		for (GameEntity gameEntity : entities) {
			gameEntity.update(deltaTime);
		}
		for (GameEntity gameEntity : toBeDestroyed) {
			entities.remove(gameEntity);
		}
		for (GameEntity gameEntity : justCreatedEntities) {
			entities.add(gameEntity);
		}
		justCreatedEntities.clear();
		toBeDestroyed.clear();
		camera.update(deltaTime);
		for (Light light : lights) {
			light.update(deltaTime);
		}
		// skybox.setPosition(new Vector3f(camera.getPosition()));
	}

	private void Render() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		final ShaderProgram shaderProgram;
		if (useSH) {
			shaderProgram = shaderSHProgram[SphericalHarmonic.bandsInUse];
		} else {
			shaderProgram = shaderGeneralProgram;
		}
		if (shaderProgram.isValid()) {
			check_gl_error("before using shader");
			shaderProgram.Use();
			check_gl_error("using shader");
			shaderProgram.setUniform("viewPosition", camera.getPosition());
			shaderProgram.setUniform("lightCount", 1);
			// putting info about lights to the shader
			// TODO make it more general, probably put struct to shader as a
			// whole
			for (int i = 0; i < lights.size() && i < 10; ++i) {
				// TODO not everything about light is passed to the shader
				shaderProgram.setUniform("lights[" + i + "].position", lights
						.get(i).getPosition());
				// TODO fix colors here of the new advanced super light
				shaderProgram.setUniform("lights[" + i + "].diffuse", lights
						.get(i).diffuse(1, 1, 1));
				shaderProgram.setUniform("lights[" + i + "].specular", lights
						.get(i).specular(1, 1, 1));
				shaderProgram.setUniform("lights[" + i + "].ambient", lights
						.get(i).ambient(1, 1, 1));
				shaderProgram.setUniform("lights[" + i + "].intensity", lights
						.get(i).getIntensity());
			}
			check_gl_error("setting up uniforms");

			shaderProgram.setUniform("density", fogDensity);
			shaderProgram.setUniform("fogColor", backgroundColor);
		}

		// postFX, render to off screen buffer. uncomment to enable
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		glClear(GL_DEPTH_BUFFER_BIT);
		for (GameEntity gameEntity : entities) {
			final ArrayList<Light> relevantLights = new ArrayList<Light>();
			for (Light light : lights) {
				if (light.getMasterEntity() == null
						|| (light.getMasterEntity() != gameEntity && entities
								.contains(light.getMasterEntity()))) {
					relevantLights.add(light);
				}
			}
			gameEntity.render(camera.getProjectionMatrix(), camera.getMatrix(),
					null, relevantLights, shaderProgram);
		}

		skyBoxShader.Use();
		skyBoxShader.setUniform("viewPosition", camera.getPosition());
		skyBoxShader.setUniform("density", 0.0f);
		skyBoxShader.setUniform("fogColor", backgroundColor);
		skybox.render(camera.getProjectionMatrix(), (Matrix4f) (MathUtils.createRotationMatrix(camera.getRotation()).invert()), null, null, skyBoxShader);

		// skyBoxShader.Use();
		// skyBoxShader.setUniform("cameraRotationMatrix",
		// Matrix4f.invert(MathUtils.createRotationMatrix(camera.getRotation()),
		// null));
		// skybox.render(camera.getProjectionMatrix(), camera.getMatrix(), null,
		// null, skyBoxShader);
		check_gl_error("rendering model");

		// postFX stuff, uncomment to enable

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		postFXShader.Use();

		postFXShader.setUniform("healthPercent",
				((TankEntity) player).getHealthPercentage());

		final Matrix4f projectionM = camera.getProjectionMatrix();
		final Matrix4f cameraM = camera.getMatrix();

		final Vector4f lightposition = new Vector4f(-1000, 0, -1000, 0);
		final Matrix4f actualMatrix = Matrix4f.mul(projectionM, cameraM, null);
		Matrix4f.transform(actualMatrix, lightposition, lightposition);
		lightposition.scale(1.0f / lightposition.w);
		Vector4f.add(lightposition, new Vector4f(1, 1, 1, 1), lightposition);
		lightposition.scale(0.5f);

		final Vector2f lightScreenPosition = new Vector2f(lightposition.x, lightposition.y);

		postFXShader.setUniform("lightPosition", lightScreenPosition);

		postFXShader.setUniform("Weight", SOL_Weight);
		postFXShader.setUniform("density", SOL_density);
		postFXShader.setUniform("NUM_SAMPLES", SOL_NUM_SAMPLES);
		postFXShader.setUniform("Decay", SOL_Decay);

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glBindTexture(GL_TEXTURE_2D, fbo_texture);
		postFXShader.setUniform("fbo_texture", 0);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
		glVertexAttribPointer(0, // attribute
				2, // number of elements per vertex, here (x,y)
				GL_FLOAT, // the type of each element
				false, // take our values as-is
				0, // no extra data between each position
				(long) 0 // offset of first element
		);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glDisableVertexAttribArray(0);

	}

	private void Cleanup() {
		AssetManager.Cleanup();
		for (int i = 0; i < 5; ++i) {
			shaderSHProgram[i].destroy();
		}
		shaderGeneralProgram.destroy();
		postFXShader.destroy();
		glDeleteRenderbuffers(rbo_depth);
		glDeleteTextures(fbo_texture);
		glDeleteFramebuffers(fbo);
		if (Display.isCreated()) {
			Display.destroy();
		}
	}

	private void Start() {
		float deltaTime = 0;
		long currentTime = 0;

		int fps_counter = 0;
		float second_timer = 0;
		while (!shouldQuit) {
			currentTime = getMiliseconds();
			deltaTime = (currentTime - lastTime) / 1000.0f;
			lastTime = currentTime;
			second_timer += deltaTime;
			if (second_timer > 1.0) {
				System.out.println("FPS: " + fps_counter);
				second_timer -= 1.0;
				fps_counter = 0;
			}
			this.Update(deltaTime);
			this.Render();
			++fps_counter;
			Display.update();
		}
	}

	public static void main(String[] argv) {
		MainACG main = new MainACG();
		System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
		System.setProperty("org.lwjgl.opengl.Display.allowSoftwareOpenGL",
				"true");
		main.Initialize(argv);
		main.Start();
		main.Cleanup();
	}

}