package com.elecfant.acg.game;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.utils.MathUtils;

public class AITankEntity extends TankEntity {

	protected TankEntity targetTank;
	protected float firingRange = 50.0f;

	public AITankEntity(float maxHealth, Vector3f tankColor) {
		super(maxHealth, tankColor);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void processInput(float elapsedTime) {
		// TODO Auto-generated method stub
		super.processInput(elapsedTime);
		if (targetTank != null && targetTank.getHealthPercentage() > 0) {
			final float targetDistance = this.getDistanceTo(targetTank);
			if (targetDistance > firingRange) {
				final Vector3f target = Vector3f.sub(targetTank.position, this.position, null);
				target.normalise();
				target.set(MathUtils.carthesianToSpherical(target));

				final Vector3f current = this.rotation;
				// TODO fix inconsistency of letters
				float dy = target.z - current.y;
				if (Math.abs(dy) > Math.PI) {
					dy = dy - (float) (Math.signum(dy) * Math.PI);
					dy *= -1;
				}
				final float smoothingFactor;
				final float smoothingThreshold = (float) Math.PI / 10;
				if (Math.abs(dy) < smoothingThreshold) {
					smoothingFactor = (float) Math.abs(dy) / (smoothingThreshold);
				} else {
					smoothingFactor = 1;
				}
				if (dy < 0) {
					this.turnLeft(elapsedTime*smoothingFactor);
				} else {
					this.turnRight(elapsedTime*smoothingFactor);
				}
				this.gasOn();

			} else {
				// TODO perform rotation of head + shoot
				final GameEntity head = this.getPart(HEAD);
				final Vector3f headPosition = new Vector3f(head.position);
				Vector3f.add(headPosition, this.position, headPosition);
				final Vector3f headRotation = new Vector3f(head.rotation);
				Vector3f.add(headRotation, this.rotation, headRotation);

				final Vector3f target = Vector3f.sub(targetTank.position, headPosition, null);
				target.y = targetTank.position.y;
				target.set(MathUtils.carthesianToSpherical(target));
				final Vector3f current = headRotation;
				// TODO fix inconsistency of letters
				target.z = (float) ((target.z + Math.PI*2) % (Math.PI*2));
				current.y = (float) ((current.y + Math.PI*2) % (Math.PI*2));
				float dy = target.z - current.y;
				if (Math.abs(dy) > Math.PI) {
					dy = dy - (float) (Math.signum(dy) * Math.PI);
					dy *= -1;
				}
				final float smoothingFactor;
				final float smoothingThreshold = (float) Math.PI / 10;
				if (Math.abs(dy) < smoothingThreshold) {
					smoothingFactor = (float) Math.abs(dy) / (smoothingThreshold);
				} else {
					smoothingFactor = 1;
				}
				if (dy < 0) {
					this.turretLeft(elapsedTime * smoothingFactor);
				} else {
					this.turretRight(elapsedTime * smoothingFactor);
				}

				this.shootCannon();
				this.gasOff();
			}

		}
	}

	public void setTarget(TankEntity targetTank) {
		this.targetTank = targetTank;
	}

}
