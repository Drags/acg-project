package com.elecfant.acg.game;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.obj.aabb.AABB;
import com.elecfant.acg.utils.ShaderProgram;

public class AmbientLight extends Light {

	private Vector3f lightColor;

	public AmbientLight(Vector3f color) {
		super();
		this.lightColor = new Vector3f(color);
		this.sphericalHarmonic = new SphericalHarmonic(this);
	}
	
	public AmbientLight(AmbientLight original) {
		super(original);
		this.lightColor = new Vector3f(original.lightColor);
	}

	@Override
	public Vector3f diffuse(float x, float y, float z) {
		final Vector3f result = new Vector3f(lightColor);
		result.scale((float)Math.pow(1f/(1-y),2));
		return result;
	}

	@Override
	public Vector3f ambient(float x, float y, float z) {
		// TODO Auto-generated method stub
		return diffuse(x, y, z);
	}

	@Override
	public Vector3f specular(float x, float y, float z) {
		// TODO Auto-generated method stub
		return diffuse(x, y, z);
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "ambient";
	}

	@Override
	public void render(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f parentMatrix, ArrayList<Light> lights, ShaderProgram shader) {
		// TODO debug rendering?
	}

	@Override
	public Light deepCopy() {
		return new AmbientLight(this);
	}

	@Override
	protected void preInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void processInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void postUpdate(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected AABB getBaseAABB() {
		return new AABB(new Vector3f(), new Vector3f());
	}

	@Override
	protected void processCollision(GameEntity gameEntity) {
		// TODO Auto-generated method stub
		
	}

}
