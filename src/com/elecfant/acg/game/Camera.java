package com.elecfant.acg.game;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.obj.aabb.AABB;
import com.elecfant.acg.utils.MathUtils;
import com.elecfant.acg.utils.ShaderProgram;

public class Camera extends GameEntity {

	protected Matrix4f projectionMatrix;
	protected GameEntity followTarget;
	protected Vector3f followDistance;
	protected boolean aimAtTarget = false;

	public Camera() {
		super();
		projectionMatrix = new Matrix4f();
	}

	public Camera(Vector3f position, Vector3f rotation) {
		super(position, rotation);
		projectionMatrix = new Matrix4f();
	}

	public Camera(Vector3f position) {
		super(position);
		projectionMatrix = new Matrix4f();
	}

	@Override
	public void update(float elapsedTime) {
		if (followTarget != null) {
			this.position.set(followTarget.position.x, followTarget.position.y, followTarget.position.z);
			this.rotation.set(followTarget.rotation.x, followTarget.rotation.y, followTarget.rotation.z);
			this.step(followDistance);
			if (aimAtTarget) {
				// this.rotation.x -= Math.PI/4;
				final Vector3f direction = Vector3f.sub(followTarget.position, this.position, null);
				if (direction.length() > 0) {
					direction.normalise();
				}
				final Vector3f newSpherical = MathUtils.carthesianToSpherical(direction);
				this.rotation.set(newSpherical.y, newSpherical.z, 0);
			}
		}
	}

	public void follow(GameEntity followTarget, Vector3f distance) {
		this.followTarget = followTarget;
		this.followDistance = distance;
	}

	public void follow(GameEntity followTarget, float distance, float transitionTime) {
		// TODO somooth transition for following target.
	}

	// TODO camera matrices works differently from models matrices, thus full
	// override
	@Override
	public Matrix4f getMatrix() {
		final Matrix4f cameraMatrix = super.getMatrix();
		cameraMatrix.invert();
		return cameraMatrix;
	}

	public Matrix4f getProjectionMatrix() {
		return this.projectionMatrix;
	}

	public void setPerspectiveProjection(int width, int height, float fov, float near, float far) {
		projectionMatrix = new Matrix4f();
		final float fieldOfView = fov;
		final float aspectRatio = (float) width / (float) height;
		final float near_plane = near;
		final float far_plane = far;
		final float y_scale = MathUtils.coTangent(MathUtils.degreesToRadians(fieldOfView / 2f));
		final float x_scale = y_scale / aspectRatio;
		final float frustum_length = far_plane - near_plane;

		projectionMatrix.m00 = x_scale;
		projectionMatrix.m11 = y_scale;
		projectionMatrix.m22 = -((far_plane + near_plane) / frustum_length);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * near_plane * far_plane) / frustum_length);
		projectionMatrix.m33 = 0;
	}

	public Vector3f getFollowingDistance() {
		return this.followDistance;
	}

	public void setFollowingDistance(Vector3f distance) {
		this.followDistance = distance;
	}

	public void unfollow() {
		this.followTarget = null;
		this.followDistance = null;
	}


	public void setAimAtTarget(boolean aimAtTarget) {
		this.aimAtTarget = aimAtTarget;
	}

	@Override
	public void render(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f parentMatrix, ArrayList<Light> lights, ShaderProgram shader) {
		//TODO debug rendering?
		return;
		
	}

	@Override
	protected void preInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void processInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void postUpdate(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected AABB getBaseAABB() {
		return new AABB(new Vector3f(), new Vector3f());
	}

	@Override
	protected void processCollision(GameEntity gameEntity) {
		// TODO Auto-generated method stub
		
	}

}
