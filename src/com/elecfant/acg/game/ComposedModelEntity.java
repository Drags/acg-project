package com.elecfant.acg.game;

import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.obj.aabb.AABB;
import com.elecfant.acg.utils.ShaderProgram;

public class ComposedModelEntity extends GameEntity {

	protected HashMap<String, GameEntity> contents;

	public ComposedModelEntity() {
		super();
		contents = new HashMap<String, GameEntity>();
	}

	public void addPart(String key, GameEntity part) {
		this.contents.put(key, part);
	}

	public GameEntity getPart(String key) {
		return this.contents.get(key);
	}

	@Override
	public void render(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f parentMatrix, ArrayList<Light> lights, ShaderProgram shader) {
		final Matrix4f modelMatrix;
		if (parentMatrix != null) {
			modelMatrix = Matrix4f.mul(parentMatrix, this.getMatrix(), null);
		} else {
			modelMatrix = this.getMatrix();
		}
		for (GameEntity gameEntity : contents.values()) {
			final ArrayList<Light> adjustedLights = new ArrayList<Light>();
			Light temp;
			for (Light light :lights) {
				temp = light.deepCopy();
				// TODO this might be inaccurate
				temp.sphericalHarmonic = temp.getAdjustedSphericalHarmonic(this);
				adjustedLights.add(temp);
			}
			gameEntity.render(projectionMatrix, viewMatrix, modelMatrix, adjustedLights, shader);
		}
	}

	/* (non-Javadoc)
	 * @see com.elecfant.acg.game.GameEntity#getMultiplyColor()
	 */
	@Override
	public Vector3f getMultiplyColor() {
		// TODO Auto-generated method stub
		return super.getMultiplyColor();
	}

	/* (non-Javadoc)
	 * @see com.elecfant.acg.game.GameEntity#setMultiplyColor(org.lwjgl.util.vector.Vector3f)
	 */
	@Override
	public void setMultiplyColor(Vector3f multiplyColor) {
		// TODO Auto-generated method stub
		super.setMultiplyColor(multiplyColor);
		for (GameEntity gameEntity : this.contents.values()) {
			gameEntity.setMultiplyColor(multiplyColor);
		}
	}

	@Override
	protected void preInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void processInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void postUpdate(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected AABB getBaseAABB() {
		// TODO Auto-generated method stub
		final AABB result = new AABB(new Vector3f(), new Vector3f());
		for (GameEntity gameEntity : this.contents.values()) {
			result.updateWith(gameEntity.getBaseAABB());
		}
		return result;
	}

	@Override
	protected void processCollision(GameEntity gameEntity) {

	}
	
	
	

}
