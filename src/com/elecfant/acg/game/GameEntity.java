package com.elecfant.acg.game;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import com.elecfant.acg.MainACG;
import com.elecfant.acg.obj.aabb.AABB;
import com.elecfant.acg.utils.MathUtils;
import com.elecfant.acg.utils.ShaderProgram;

public abstract class GameEntity {

	protected Vector3f position;
	protected Vector3f speed;
	protected Vector3f rotation;
	protected Vector3f scale;
	protected boolean applyPhysics = false;

	protected Vector3f multiplyColor = new Vector3f(1, 1, 1);

	public final static float PI2 = (float) (Math.PI * 2);

	public GameEntity() {
		this.position = new Vector3f();
		this.rotation = new Vector3f();
		this.speed = new Vector3f();
		this.scale = new Vector3f(1.0f, 1.0f, 1.0f);
		this.multiplyColor = new Vector3f(1, 1, 1);
	}

	public GameEntity(Vector3f position) {
		this();
		this.position.set(position.x, position.y, position.z);
	}

	public GameEntity(Vector3f position, Vector3f rotation) {
		this(position);
		this.rotation.set(rotation.x, rotation.y, rotation.z);
	}

	public GameEntity(Vector3f position, Vector3f rotation, Vector3f scale) {
		this(position, rotation);
		this.scale.set(scale.x, scale.y, scale.z);
	}

	public GameEntity(GameEntity original) {
		this.position = new Vector3f(original.position);
		this.scale = new Vector3f(original.scale);
		this.rotation = new Vector3f(original.rotation);
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position.set(position);
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
	}

	public void setScale(float d) {
		this.scale = new Vector3f(d, d, d);
	}

	public void setScale(Vector3f d) {
		this.scale.set(d.x, d.y, d.z);
	}

	public Vector3f getScale() {
		return this.scale;
	}

	public void yaw(float angle) {
		this.rotation.y = (this.rotation.y + PI2 + angle) % PI2;
	}

	public void pitch(float angle) {
		this.rotation.x = (this.rotation.x + PI2 + angle) % PI2;
	}

	public void roll(float angle) {
		this.rotation.z = (this.rotation.z + PI2 + angle) % PI2;
	}

	public void stepForward(float step) {
		this.step(new Vector3f(0, 0, -1), step);
	}

	public void stepBackward(float step) {
		this.step(new Vector3f(0, 0, 1), step);
	}

	public void step(Vector3f step) {
		this.step(new Vector3f(0, 0, 1), step.z);
		this.step(new Vector3f(1, 0, 0), step.x);
		this.step(new Vector3f(0, 1, 0), step.y);
	}

	public void stepLeft(float step) {
		this.step(new Vector3f(-1, 0, 0), step);
	}

	public void stepRight(float step) {
		this.step(new Vector3f(1, 0, 0), step);
	}

	public void stepUp(float step) {
		this.step(new Vector3f(0, 1, 0), step);

	}

	public void stepDown(float step) {
		this.step(new Vector3f(0, -1, 0), step);
	}

	protected void step(Vector3f direction, float step) {
		final Matrix4f rotationMat = MathUtils.createRotationMatrix(this.rotation);
		final Vector4f temp_step = new Vector4f(direction.x, direction.y, direction.z, 1);
		Matrix4f.transform(rotationMat, temp_step, temp_step);
		this.position.x += step * temp_step.x;
		this.position.y += step * temp_step.y;
		this.position.z += step * temp_step.z;
	}

	public Matrix4f getMatrix() {

		final Matrix4f matrix = new Matrix4f();
		matrix.translate(this.position);
		matrix.scale(this.scale);
		Matrix4f.mul(matrix, MathUtils.createRotationMatrix(this.rotation), matrix);
		return matrix;
	}

	public abstract void render(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f parentMatrix, ArrayList<Light> lights, ShaderProgram shader);

	public void update(float elapsedTime) {
		this.preInput(elapsedTime);
		this.processInput(elapsedTime);
		this.updatePhysics(elapsedTime);
		for (GameEntity gameEntity : MainACG.entities) {
			if (gameEntity == this) {
				continue;
			}
			if (this.isColliding(gameEntity) != null) {
				this.processCollision(gameEntity);
			}
		}
		this.postUpdate(elapsedTime);
	}

	protected abstract void processCollision(GameEntity gameEntity);

	protected abstract void preInput(float elapsedTime);

	protected abstract void processInput(float elapsedTime);

	protected abstract void postUpdate(float elapsedTime);

	protected void updatePhysics(float elapsedTime) {
		if (this.applyPhysics && PhysicsEnvironment.physicsEnabled) {

			final Vector3f gravityContribution = new Vector3f(PhysicsEnvironment.G);
			gravityContribution.scale(elapsedTime);

			Vector3f.add(this.speed, gravityContribution, this.speed);

			final Matrix4f rotationMatrix = this.getMatrix();
			final Vector4f deltaPosition4 = new Vector4f(this.speed.x, this.speed.y, this.speed.z, 0);
			Matrix4f.transform(rotationMatrix, deltaPosition4, deltaPosition4);

			final Vector3f deltaPosition = new Vector3f(deltaPosition4.x, deltaPosition4.y, deltaPosition4.z);
			deltaPosition.scale(elapsedTime);
			Vector3f.add(this.position, deltaPosition, this.position);

			if (this.position.y < 0) {
				this.speed.y = 0;
			}

			this.clampPosition(PhysicsEnvironment.boundingBox);
		}
	}

	public void clampPosition(AABB boundingBox) {
		if (boundingBox == null) {
			return;
		}
		if (this.position.x > boundingBox.max.x) {
			this.position.x = boundingBox.max.x;
		}
		if (this.position.x < boundingBox.min.x) {
			this.position.x = boundingBox.min.x;
		}
		if (this.position.y > boundingBox.max.y) {
			this.position.y = boundingBox.max.y;
		}
		if (this.position.y < boundingBox.min.y) {
			this.position.y = boundingBox.min.y;
		}
		if (this.position.z > boundingBox.max.z) {
			this.position.z = boundingBox.max.z;
		}
		if (this.position.z < boundingBox.min.z) {
			this.position.z = boundingBox.min.z;
		}

	}

	public void enablePhysics() {
		this.applyPhysics = true;
	}

	public void disablePhysics() {
		this.applyPhysics = false;
	}

	public float getDistanceTo(GameEntity target) {
		final Vector3f result = new Vector3f();
		Vector3f.sub(target.position, this.position, result);
		return result.length();
	}

	public void setSpeed(Vector3f speed) {
		this.speed.set(speed);
	}

	/**
	 * @return the multiplyColor
	 */
	public Vector3f getMultiplyColor() {
		return multiplyColor;
	}

	/**
	 * @param multiplyColor
	 *            the multiplyColor to set
	 */
	public void setMultiplyColor(Vector3f multiplyColor) {
		this.multiplyColor = new Vector3f(multiplyColor);
	}

	protected abstract AABB getBaseAABB();

	public Vector3f[] getCollisionBox() {
		final Vector3f[] all = this.getBaseAABB().getAllVertices();
		Matrix4f rotationMatrix = this.getMatrix();
		final Vector4f[] all4 = new Vector4f[8];
		for (int i = 0; i < 8; ++i) {
			all4[i] = new Vector4f(all[i].x, all[i].y, all[i].z, 1.0f);
			Matrix4f.transform(rotationMatrix, all4[i], all4[i]);
			all[i].set(all4[i].x, all4[i].y, all4[i].z);
		}
		return all;
	}

	public Vector3f isColliding(GameEntity other) {
		final float distance = Vector3f.sub(this.position, other.position, null).length();
		if (distance > this.getBaseAABB().maxDistance() / 2 + other.getBaseAABB().maxDistance() / 2) {
			return null;
		} else {
			return new Vector3f();
		}
	}

}
