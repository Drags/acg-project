package com.elecfant.acg.game;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.utils.MathUtils;

abstract public class Light extends GameEntity {

	public final static Vector3f BLACK_COLOR = new Vector3f(0, 0, 0);

	protected GameEntity masterEntity = null;

	public Light() {
		super();
	}

	public Light(Vector3f position, Vector3f rotation, Vector3f scale) {
		super(position, rotation, scale);
	}

	public Light(Vector3f position, Vector3f rotation) {
		super(position, rotation);
	}

	public Light(Vector3f position) {
		super(position);
	}

	public Light(Light original) {
		super(original);
		this.sphericalHarmonic = new SphericalHarmonic(original.sphericalHarmonic);
	}

	protected SphericalHarmonic sphericalHarmonic;

	abstract public Vector3f diffuse(float x, float y, float z);

	abstract public Vector3f ambient(float x, float y, float z);

	abstract public Vector3f specular(float x, float y, float z);

	abstract public String getType();

	public SphericalHarmonic getSphericalHarmonic() {
		if (this.sphericalHarmonic == null)
			return null;
		return this.sphericalHarmonic.getRotated(this.rotation);
	}

	public void rotate(float theta, float phi, float gamma) {
		Vector3f.add(this.rotation, new Vector3f(theta, phi, gamma), this.rotation);
	}

	public float getIntensity() {
		return scale.length();
	}

	public SphericalHarmonic getAdjustedSphericalHarmonic(GameEntity gameEntity) {
		final SphericalHarmonic result = this.getSphericalHarmonic();

		final Vector3f dirToLight = new Vector3f();
		Vector3f.sub(this.position, gameEntity.position, dirToLight);
		// TODO checking if masterEntity == null is faulty, need to think of something better
		if (dirToLight.length() > 0 && this.masterEntity == null) {
			dirToLight.normalise();
			final Vector3f sphericalCoords = MathUtils.carthesianToSpherical(dirToLight);
			sphericalCoords.scale(-1.0f);
			result.rotate(new Vector3f(sphericalCoords.y, sphericalCoords.z, 0));
			result.rotate(new Vector3f(0, gameEntity.rotation.y, 0));
			result.rotate(new Vector3f(gameEntity.rotation.x, 0, 0));
		}
		return result;
	}

	public abstract Light deepCopy();

	public GameEntity getMasterEntity() {
		return this.masterEntity;
	}

	public void attatchTo(GameEntity master) {
		this.masterEntity = master;
	}

	@Override
	public void update(float elapsedTime) {
		// TODO Auto-generated method stub
		super.update(elapsedTime);
		if (this.masterEntity != null) {
			this.position.set(this.masterEntity.position);
			Vector3f.add(this.position, (Vector3f) this.masterEntity.getBaseAABB().getCenter().scale(0.5f), this.position);
		}
	}

}
