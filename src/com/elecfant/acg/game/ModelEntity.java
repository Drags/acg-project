package com.elecfant.acg.game;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;

import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.obj.OBJModel;
import com.elecfant.acg.obj.Ray;
import com.elecfant.acg.obj.aabb.AABB;
import com.elecfant.acg.utils.ShaderProgram;

public class ModelEntity extends GameEntity {

	protected OBJModel model;

	public ModelEntity(OBJModel model) {
		super();
		this.model = model;
	}

	public boolean intersect(Ray r) {
		return this.model.intersect(r);
	}

	@Override
	public void render(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f parentMatrix, ArrayList<Light> lights, ShaderProgram shader) {
		// TODO start using shader here instead of outside model class
		final Matrix4f modelMatrix;
		if (parentMatrix != null) {
			modelMatrix = Matrix4f.mul(parentMatrix, this.getMatrix(), null);
		} else {
			modelMatrix = this.getMatrix();
		}

		if (lights != null) {
			shader.setUniform("totalLights", lights.size());
			for (int j = 0; j < lights.size(); ++j) {
				final SphericalHarmonic shForGameEntity = lights.get(j).getAdjustedSphericalHarmonic(this);
				for (int i = 0; i < SphericalHarmonic.coeficientsInUse; ++i) {
					shader.setUniform("lights[" + j + "].coefs[" + i + "]", shForGameEntity.getCoeficient(i));
				}
				shader.setUniform("lights[" + j + "].position", lights.get(j).position);
				shader.setUniform("lights[" + j + "].intensity", lights.get(j).getIntensity());
			}
		} else {
			shader.setUniform("totalLights", 0);
		}

		final Matrix4f viewModelMatrix = Matrix4f.mul(viewMatrix, modelMatrix, null);
		final Matrix4f projectionViewModelMatrix = Matrix4f.mul(projectionMatrix, viewModelMatrix, null);
		final Matrix4f normalMatrix = new Matrix4f(modelMatrix);

		shader.setUniform("multiplyColor", multiplyColor);
		shader.setUniform("normalMatrix", normalMatrix);
		shader.setUniform("viewModelMatrix", viewModelMatrix);
		shader.setUniform("projectionViewModelMatrix", projectionViewModelMatrix);
		shader.setUniform("modelMatrix", modelMatrix);

		this.model.render();
	}

	@Override
	protected void preInput(float elapsedTime) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void processInput(float elapsedTime) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void postUpdate(float elapsedTime) {
		// TODO Auto-generated method stub

	}

	@Override
	protected AABB getBaseAABB() {
		return this.model.getAABB();
	}

	@Override
	protected void processCollision(GameEntity gameEntity) {

	}

}
