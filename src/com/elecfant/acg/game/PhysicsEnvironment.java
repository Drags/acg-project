package com.elecfant.acg.game;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.obj.aabb.AABB;

public class PhysicsEnvironment {
	public static Vector3f G = new Vector3f(0,-10,0);
	public static boolean physicsEnabled = true;
	public static AABB boundingBox = new AABB(
			new Vector3f(-100, 0, -88),
			new Vector3f(150, 300, 86)
			);
}
