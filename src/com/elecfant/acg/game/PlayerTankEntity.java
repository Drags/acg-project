package com.elecfant.acg.game;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

public class PlayerTankEntity extends TankEntity {

	public PlayerTankEntity(float maxHealth, Vector3f tankColor) {
		super(maxHealth, tankColor);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.elecfant.acg.game.TankEntity#processInput(float)
	 */
	@Override
	protected void processInput(float elapsedTime) {
		
		
		super.processInput(elapsedTime);
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			this.gasOn();
		} else if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			this.gasOnRev();
		} else {
			this.gasOff();
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			this.turnLeft(elapsedTime);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			this.turnRight(elapsedTime);
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_B)) {
			this.turretLeft(elapsedTime);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_N)) {
			this.turretRight(elapsedTime);
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			this.shootCannon();
		}
	}

}
