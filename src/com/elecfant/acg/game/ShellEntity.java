package com.elecfant.acg.game;

import com.elecfant.acg.AssetManager;
import com.elecfant.acg.MainACG;

public class ShellEntity extends ModelEntity {
	
	public final static float LIFE_TIME = 5.0f;
	protected float lifetime = 0;
	protected boolean armed = false;
	protected float damage = 25.0f;
	
	final public static float armTime = 0.02f;
	
	public ShellEntity() {
		super(AssetManager.get("obj/[prepared]/shell.obj"));
	}

	@Override
	protected void preInput(float elapsedTime) {
		super.preInput(elapsedTime);
		lifetime += elapsedTime;
		if (lifetime > armTime) {
			armed = true;
		}
	}
	
	@Override
	protected void postUpdate(float elapsedTime) {
		if (lifetime > LIFE_TIME || this.speed.length() < 0.01) {
			MainACG.toBeDestroyed.add(this);
		}
	}

	/* (non-Javadoc)
	 * @see com.elecfant.acg.game.ModelEntity#processCollision(com.elecfant.acg.game.GameEntity)
	 */
	@Override
	protected void processCollision(GameEntity gameEntity) {
		if (!(gameEntity instanceof TankEntity) || !armed ) {
			return;
		}
		System.out.println("target down");
		final TankEntity targetTank = (TankEntity) gameEntity;
		targetTank.inflictDamage(this.damage);
		MainACG.toBeDestroyed.add(this);
	}
	
	
}
