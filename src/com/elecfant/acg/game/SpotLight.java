package com.elecfant.acg.game;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.obj.aabb.AABB;
import com.elecfant.acg.utils.ShaderProgram;

public class SpotLight extends Light {

	protected Vector3f lightColor;
	protected float cutoff;

	public SpotLight(Vector3f position, Vector3f rotation, Vector3f scale, Vector3f color, float cutoff) {
		super(position, rotation, scale);
		this.lightColor = new Vector3f(color);
		this.cutoff = cutoff;
		this.sphericalHarmonic = new SphericalHarmonic(this);
	}
	
	public SpotLight(Vector3f color, float cutoff) {
		super();
		this.lightColor = new Vector3f(color);
		this.cutoff = cutoff;
		this.sphericalHarmonic = new SphericalHarmonic(this);
	}

	public SpotLight(SpotLight original) {
		super(original);
		this.lightColor = new Vector3f(original.lightColor);
		this.cutoff = original.cutoff;
	}

	@Override
	public Vector3f diffuse(float x, float y, float z) {
//		return new Vector3f(x > cutoff ? x:0, y > cutoff ? y:0, z > cutoff ? z:0);
		if (y > cutoff)
			return lightColor;
		return BLACK_COLOR;
	}

	@Override
	public Vector3f ambient(float x, float y, float z) {
		if (y > cutoff) {
			return (Vector3f) new Vector3f(lightColor).scale(5.0f);
		}
		return BLACK_COLOR;
	}

	@Override
	public Vector3f specular(float x, float y, float z) {
		if (y > cutoff)
			return (Vector3f) new Vector3f(lightColor).scale(500.0f);
		return BLACK_COLOR;
	}

	@Override
	public String getType() {
		return "spotlight";
	}

	@Override
	public void render(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f parentMatrix, ArrayList<Light> lights, ShaderProgram shader) {
		// TODO debuging rendering?
		return;
	}

	@Override
	public Light deepCopy() {
		return new SpotLight(this);
	}

	@Override
	protected void preInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void processInput(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void postUpdate(float elapsedTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected AABB getBaseAABB() {
		return new AABB(new Vector3f(), new Vector3f());
	}

	@Override
	protected void processCollision(GameEntity gameEntity) {
		// TODO Auto-generated method stub
		
	}

}
