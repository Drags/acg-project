package com.elecfant.acg.game;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import com.elecfant.acg.AssetManager;
import com.elecfant.acg.MainACG;
import com.elecfant.acg.utils.MathUtils;
import com.elecfant.acg.utils.ShaderProgram;

public class TankEntity extends ComposedModelEntity {
	protected float MAX_HEALTH;
	protected float current_Health;
	protected Vector3f tankColor;
	
	protected boolean canFire = true;

	public static final float rotation_speed = (float) (Math.PI / 8);
	protected float max_speed = 25;

	protected final static String BODY = "tank-body";
	protected final static String HEAD = "tank-head";

	private static final float WPN_COOLDOWN = 1.0f;
	private float wpn = 0;

	public TankEntity(float maxHealth, Vector3f tankColor) {
		super();
		final ModelEntity tankBody = new ModelEntity(AssetManager.get("obj/[prepared]/tank-body.obj"));
		final ModelEntity tankHead = new ModelEntity(AssetManager.get("obj/[prepared]/tank-head.obj"));
		tankHead.setPosition(new Vector3f(0, 3.9f, -1.0f));
		this.contents.put(BODY, tankBody);
		this.contents.put(HEAD, tankHead);
		this.MAX_HEALTH = maxHealth;
		this.current_Health = maxHealth;
		this.tankColor = new Vector3f(tankColor);
		this.setMultiplyColor(tankColor);
	}

	public float getHealthPercentage() {
		return this.current_Health / this.MAX_HEALTH;
	}

	@Override
	public void render(Matrix4f projectionMatrix, Matrix4f viewMatrix, Matrix4f parentMatrix, ArrayList<Light> lights, ShaderProgram shader) {
		final Vector3f currentColor = new Vector3f(this.tankColor);
		currentColor.scale(getHealthPercentage());
		this.setMultiplyColor(currentColor);
		super.render(projectionMatrix, viewMatrix, parentMatrix, lights, shader);
	}

	@Override
	protected void preInput(float elapsedTime) {
		super.preInput(elapsedTime);
		wpn -= elapsedTime;
	}

	@Override
	protected void processInput(float elapsedTime) {
		super.processInput(elapsedTime);
	}
	
	public void enableCannon() {
		canFire = true;
	}
	
	public void disableCannon() {
		canFire = false;
	}
	
	public void shootCannon() {
		if (wpn <= 0 && canFire) {
			final Vector4f headPosition = new Vector4f(
					this.getPart("tank-head").getPosition().x,
					this.getPart("tank-head").getPosition().y,
					this.getPart("tank-head").getPosition().z,
					1.0f
					);
			headPosition.y += 1.0f;
			final Vector3f headRotation = this.getPart("tank-head").getRotation();
			final Vector3f tankPosition = this.getPosition();
			final Vector3f tankRotation = this.getRotation();
			Matrix4f.transform(MathUtils.createRotationMatrix(tankRotation), headPosition, headPosition);
			final Vector3f realHeadPosition = new Vector3f(headPosition.x, headPosition.y, headPosition.z);
			final Vector3f shellPosition = new Vector3f();
			Vector3f.add(tankPosition, realHeadPosition, shellPosition);
			final Vector3f shellRotation = new Vector3f();
			Vector3f.add(tankRotation, headRotation, shellRotation);
			final ModelEntity shell = new ShellEntity();
			shell.enablePhysics();
			shell.setPosition(shellPosition);
			shell.setRotation(shellRotation);
			shell.setMultiplyColor(new Vector3f(this.multiplyColor));

			shell.setSpeed(new Vector3f(0, 0, -500));
			MainACG.justCreatedEntities.add(shell);
			wpn = WPN_COOLDOWN;
		}
	}
	
	protected void shootCannonBlank() {
		if (wpn <= 0) {
			final Vector4f headPosition = new Vector4f(
					this.getPart("tank-head").getPosition().x,
					this.getPart("tank-head").getPosition().y,
					this.getPart("tank-head").getPosition().z,
					1.0f
					);
			headPosition.y += 1.0f;
			final Vector3f headRotation = this.getPart("tank-head").getRotation();
			final Vector3f tankPosition = this.getPosition();
			final Vector3f tankRotation = this.getRotation();
			Matrix4f.transform(MathUtils.createRotationMatrix(tankRotation), headPosition, headPosition);
			final Vector3f realHeadPosition = new Vector3f(headPosition.x, headPosition.y, headPosition.z);
			final Vector3f shellPosition = new Vector3f();
			Vector3f.add(tankPosition, realHeadPosition, shellPosition);
			final Vector3f shellRotation = new Vector3f();
			Vector3f.add(tankRotation, headRotation, shellRotation);
			System.out.println(this + " shoot ");
			wpn = WPN_COOLDOWN;
		}
	}

	public void gasOn() {
		this.speed.z = -max_speed;
	}

	public void gasOff() {
		this.speed.z = 0;
	}

	public void gasOnRev() {
		this.speed.z = max_speed;
	}

	public void turnRight(float elapsedTime) {
		this.yaw(-rotation_speed * elapsedTime);
	}

	public void turnLeft(float elapsedTime) {
		this.turnRight(-elapsedTime);
	}

	public void turretLeft(float elapsedTime) {
		this.getPart("tank-head").yaw(elapsedTime * rotation_speed);
	}

	public void turretRight(float elapsedTime) {
		this.turretLeft(-elapsedTime);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.elecfant.acg.game.ComposedModelEntity#processCollision(com.elecfant
	 * .acg.game.GameEntity)
	 */
	@Override
	protected void processCollision(GameEntity gameEntity) {
		if (!(gameEntity instanceof TankEntity)) {
			return;
		}
		final float currentDistance = this.getDistanceTo(gameEntity);
		final float minimumDistance = this.getBaseAABB().maxDistance() / 2 + gameEntity.getBaseAABB().maxDistance() / 2;
		final Vector3f direction = Vector3f.sub(this.position, gameEntity.position, null);
		if (direction.length() > 0) {
			direction.normalise();
		} else {
			direction.set(0,0,1);
		}
		this.step(direction, minimumDistance-currentDistance);
	}

	public void inflictDamage(float damage) {
		this.current_Health -= damage;
		if (this.getHealthPercentage() <= 0) {
			MainACG.toBeDestroyed.add(this);
		}
	}

}
