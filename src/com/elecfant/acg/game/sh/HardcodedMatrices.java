package com.elecfant.acg.game.sh;

public class HardcodedMatrices {
	public static float[][] getBandRotationY90NegMatrix(int bandNumber) {
		
		final int coeficientsPerBand = SphericalHarmonic.getCoeficientsPerBand(bandNumber);
		final float[][] result = new float[coeficientsPerBand][coeficientsPerBand];
		switch (bandNumber) {
		case 1:
			result[0][0] = 1;
			result[1][2] = 1;
			result[2][1] = -1;
			break;
		case 2:
			result[0][1] = -1;
			result[1][0] = 1;
			result[2][2] = -0.5f;
			result[2][4] = 0.8660254f;
			result[3][3] = -1;
			result[4][2] = 0.8660254f;
			result[4][4] = 0.5f;
			break;
		case 3:
			result[0][0] = 0.25f;
			result[0][2] = 0.96824584f;
			result[1][1] = -1;
			result[2][0] = 0.96824584f;
			result[2][2] = -0.25f;
			result[3][4] = -0.61237244f;
			result[3][6] = 0.79056942f;
			result[4][3] = 0.61237244f;
			result[4][5] = -0.79056942f;
			result[5][4] = 0.79056942f;
			result[5][6] = 0.61237244f;
			result[6][3] = -0.79056942f;
			result[6][5] = -0.61237244f;
			break;
		case 0:
		default:
			result[0][0] = 1;
			break;
		}
		
		return result;
	}

	public static float[][] getBandRotationY90PosMatrix(int bandNumber) {
		
		final int coeficientsPerBand = SphericalHarmonic.getCoeficientsPerBand(bandNumber);
		final float[][] result = new float[coeficientsPerBand][coeficientsPerBand];
		switch (bandNumber) {
		case 1:
			result[0][0] = 1;
			result[1][2] = -1;
			result[2][1] = 1;
			break;
		case 2:
			result[0][1] = 1;
			result[1][0] = -1;
			result[2][2] = -0.5f;
			result[2][4] = 0.8660254f;
			result[3][3] = -1;
			result[4][2] = 0.8660254f;
			result[4][4] = 0.5f;
			break;
		case 3:
			result[0][0] = 0.25f;
			result[0][2] = 0.96824584f;
			result[1][1] = -1;
			result[2][0] = 0.96824584f;
			result[2][2] = -0.25f;
			result[3][4] = 0.61237244f;
			result[3][6] = -0.79056942f;
			result[4][3] = -0.61237244f;
			result[4][5] = 0.79056942f;
			result[5][4] = -0.79056942f;
			result[5][6] = -0.61237244f;
			result[6][3] = 0.79056942f;
			result[6][5] = 0.61237244f;
			break;
		case 0:
		default:
			result[0][0] = 1;
			break;
		}
		
		return result;
	}

}
