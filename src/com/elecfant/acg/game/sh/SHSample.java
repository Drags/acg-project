package com.elecfant.acg.game.sh;

import org.lwjgl.util.vector.Vector3f;

public class SHSample {
	public Vector3f spherical;
	public Vector3f carthesian;
	public double[] coeff = new double[SphericalHarmonic.coeficientsLimit];
}
