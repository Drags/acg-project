package com.elecfant.acg.game.sh;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.game.Light;
import com.elecfant.acg.obj.IntersectionPoint;
import com.elecfant.acg.obj.Material;
import com.elecfant.acg.obj.OBJModel;
import com.elecfant.acg.obj.Ray;
import com.elecfant.acg.obj.Vertex;
import com.elecfant.acg.utils.MathUtils;

/**
 * @author Drags
 * 
 */
public class SphericalHarmonic {

	// global parameters and limits
	public static int bandLimit = -1;
	public static int bandsInUse = -1;
	public static int coeficientsLimit = -1;
	public static int coeficientsInUse = -1;

	public static int interreflectionBounces = 1;

	public final static float interreflectionDecay = 0.1f;
	public final static float RAYTRACE_EPSILON = 0.0001f;

	public static SHSample[] samples;

	// instance members
	protected Vector3f[] transferFunction;
	protected Vector3f[] interreflectionFunction;
	protected boolean[] sampleOccluded; // if the sample is occluded by geometry

	static public void Initialize(int sqrt_samples, int bandLimit) {

		// initialize global parameters
		SphericalHarmonic.bandLimit = bandLimit;
		SphericalHarmonic.coeficientsLimit = bandLimit * bandLimit;

		// initialize default "in use" variables
		SphericalHarmonic.bandsInUse = 0;
		SphericalHarmonic.coeficientsInUse = 0;

		// initialize cached samples
		samples = new SHSample[sqrt_samples * sqrt_samples];
		// fill an N*N array with uniformly distributed
		// samples across the sphere using jittered stratification

		int i = 0; // array index
		double oneoverN = 1.0 / sqrt_samples;
		for (int a = 0; a < sqrt_samples; a++) {
			for (int b = 0; b < sqrt_samples; b++) {
				double x = (a + Math.random()) * oneoverN; // do not reuse
															// results
				double y = (b + Math.random()) * oneoverN; // each sample must
															// be random
				double theta = 2.0 * Math.acos(Math.sqrt(1.0 - x));
				double phi = 2.0 * Math.PI * y;
				samples[i] = new SHSample();
				samples[i].spherical = new Vector3f((float) theta, (float) phi, 1.0f);
				// convert spherical coords to unit vector
				Vector3f carthesian = new Vector3f((float) (Math.sin(theta) * Math.cos(phi)), (float) Math.cos(theta), (float) (Math.sin(theta) * Math.sin(phi)));
				samples[i].carthesian = carthesian;
				// precompute all SH coefficients for this sample
				for (int l = 0; l < SphericalHarmonic.bandLimit; ++l) {
					for (int m = -l; m <= l; ++m) {
						int index = (l * (l + 1) + m);
						samples[i].coeff[index] = SH(l, m, theta, phi);
					}
				}
				++i;
			}
		}
	}

	static private double SH(int l, int m, double theta, double phi)
	{
		// return a point sample of a Spherical Harmonic basis function
		// l is the band, range [0..N]
		// m in the range [-l..l]
		// theta in the range [0..Pi]
		// phi in the range [0..2*Pi]
		final double sqrt2 = Math.sqrt(2.0);
		if (m == 0)
			return K(l, 0) * P(l, m, Math.cos(theta));
		else if (m > 0)
			return sqrt2 * K(l, m) * Math.cos(m * phi) * P(l, m, Math.cos(theta));
		else
			return sqrt2 * K(l, -m) * Math.sin(-m * phi) * P(l, -m, Math.cos(theta));
	}

	// function for evaluating an Associated Legendre Polynomial P(l,m,x) at x
	static private double P(int l, int m, double x) {
		double pmm = 1.0;
		if (m > 0) {
			double somx2 = Math.sqrt((1.0 - x) * (1.0 + x));
			double fact = 1.0;
			for (int i = 1; i <= m; i++) {
				pmm *= (-fact) * somx2;
				fact += 2.0;
			}
		}
		if (l == m)
			return pmm;
		double pmmp1 = x * (2.0 * m + 1.0) * pmm;
		if (l == m + 1)
			return pmmp1;
		double pll = 0.0;
		for (int ll = m + 2; ll <= l; ++ll) {
			pll = ((2.0 * ll - 1.0) * x * pmmp1 - (ll + m - 1.0) * pmm) / (ll - m);
			pmm = pmmp1;
			pmmp1 = pll;
		}
		return pll;
	}

	// renormalisation constant for SH function
	static private double K(int l, int m) {
		double temp = ((2.0 * l + 1.0) * factorial(l - m)) / (4.0 * Math.PI * factorial(l + m));
		return Math.sqrt(temp);
	}

	// naive factorial function required for SH calculations
	static private double factorial(int i) {
		switch (i) {
		case 0:
		case 1:
			return 1;
		case 2:
			return 2;
		case 3:
			return 6;
		case 4:
			return 24;
		case 5:
			return 120;
		case 6:
			return 720;
		case 7:
			return 5040;
		case 8:
			return 40320;
		default:
			// fail-safe, just in case we need higher factorials
			return factorial(i - 1) * i;
		}
	}

	// constructor to construct SH for vertex occlusion (and later
	// interreflection)
	public SphericalHarmonic(Vertex vertex, Material material, OBJModel objModel) {
		if (!SphericalHarmonic.isInitialized())
			return;
		// for each sample
		double value;
		final Vector3f result[] = new Vector3f[coeficientsLimit];
		for (int i = 0; i < coeficientsLimit; ++i) {
			result[i] = new Vector3f();
		}
		this.sampleOccluded = new boolean[samples.length];
		for (int i = 0; i < samples.length; ++i) {
			// calculate cosine term for this sample
			double H = Vector3f.dot(samples[i].carthesian, vertex.normal);
			if (H > 0) {
				// ray inside upper hemisphere so...
				final Ray r = new Ray();
				final Vector3f d = new Vector3f(vertex.normal);
				d.normalise();
				d.scale(SphericalHarmonic.RAYTRACE_EPSILON);
				r.origin = new Vector3f(vertex.position);
				Vector3f.add(r.origin, d, r.origin);
				r.direction = new Vector3f(samples[i].carthesian);
				if (!objModel.intersect(r)) {
					this.sampleOccluded[i] = false;
					// SH project over all bands into the sum vector
					for (int j = 0; j < SphericalHarmonic.coeficientsLimit; ++j) {
						value = H * samples[i].coeff[j];
						result[j].x += value;
						result[j].y += value;
						result[j].z += value;
					}
				} else {
					this.sampleOccluded[i] = true;
				}
			}
		}
		// divide the result by probability / number of samples
		final Vector3f actualDiffuseMaterial = (material != null ? material.Kd : new Vector3f(1, 1, 1));
		final double factor = Math.PI * 2 / SphericalHarmonic.coeficientsLimit;
		for (int i = 0; i < SphericalHarmonic.coeficientsLimit; ++i) {
			result[i].x = (float) (result[i].x * factor * actualDiffuseMaterial.x);
			result[i].y = (float) (result[i].y * factor * actualDiffuseMaterial.y);
			result[i].z = (float) (result[i].z * factor * actualDiffuseMaterial.z);
		}

		this.transferFunction = result;
	}

	// deep copy of spherical harmonic
	public SphericalHarmonic(SphericalHarmonic sphericalHarmonic) {
		this.transferFunction = new Vector3f[coeficientsLimit];
		for (int i = 0; i < coeficientsLimit; ++i) {
			this.transferFunction[i] = new Vector3f(sphericalHarmonic.transferFunction[i]);
		}
		if (sphericalHarmonic.interreflectionFunction != null) {
			this.interreflectionFunction = new Vector3f[coeficientsLimit];
			for (int i = 0; i < coeficientsLimit; ++i) {
				this.interreflectionFunction[i] = new Vector3f(sphericalHarmonic.interreflectionFunction[i]);
			}
		}
	}

	public SphericalHarmonic(float unifiedCoeficient) {
		this.transferFunction = new Vector3f[coeficientsLimit];
		for (int i = 0; i < coeficientsInUse; ++i) {
			this.transferFunction[i] = new Vector3f(unifiedCoeficient,unifiedCoeficient, unifiedCoeficient);
		}
	}

	public SphericalHarmonic(Light light) {
		final Vector3f[] result = new Vector3f[coeficientsLimit];
		for (int i = 0; i < coeficientsLimit; ++i) {
			result[i] = new Vector3f();
		}
		final float weight = (float) (4.0 * Math.PI);
		// for each sample
		for (int i = 0; i < samples.length; ++i) {
			final Vector3f lightIntensity = light.diffuse(samples[i].carthesian.x, samples[i].carthesian.y, samples[i].carthesian.z);
			for (int n = 0; n < coeficientsLimit; ++n) {
				result[n].x += lightIntensity.x * samples[i].coeff[n];
				result[n].y += lightIntensity.y * samples[i].coeff[n];
				result[n].z += lightIntensity.z * samples[i].coeff[n];
			}
		}
		// divide the result by weight and number of samples
		float factor = weight / samples.length;
		for (int i = 0; i < result.length; ++i) {
			result[i].scale(factor);
		}
		this.transferFunction = result;
	}

	public SphericalHarmonic(float[] coeficients) {
		this.transferFunction = new Vector3f[coeficientsLimit];
		for (int i = 0; i < coeficientsLimit; ++i) {
			this.transferFunction[i] = new Vector3f(
					coeficients[3 * i],
					coeficients[3 * i + 1],
					coeficients[3 * i + 2]
					);
		}
	}

	// method for calculating interreflection for vertex
	// TODO combine interreflection with transfer after interreflecting
	// TODO remove occlusion flag array after interreflection
	public void calculateInterreflection(Vertex vertex, OBJModel obj, int bounce) {
		this.interreflectionFunction = new Vector3f[coeficientsLimit];
		final Vector3f[] destination = new Vector3f[coeficientsLimit];

		for (int i = 0; i < coeficientsLimit; ++i) {
			this.interreflectionFunction[i] = new Vector3f();
			destination[i] = new Vector3f();
		}

		for (int i = 0; i < samples.length; ++i) {

			// check if sample was occluded in first (shading) pass
			if (!this.sampleOccluded[i])
				continue;
			// calculate cosine term for this sample
			float H = Vector3f.dot(samples[i].carthesian, vertex.normal);
			if (H > 0.0) {
				// ray inside upper hemisphere so...
				final Ray r = new Ray();
				final Vector3f d = new Vector3f(vertex.normal);
				d.normalise();
				d.scale(RAYTRACE_EPSILON);
				r.origin = new Vector3f(vertex.position);
				Vector3f.add(r.origin, d, r.origin);
				r.direction = samples[i].carthesian;
				IntersectionPoint ip = obj.getFirstIntersection(r);
				if (ip == null)
					continue;
				final float distance = Vector3f.sub(ip.point, r.origin, null).length();
				float reachedIntensity = (float) Math.pow(Math.E, -SphericalHarmonic.interreflectionDecay * distance);
				final Vertex[] vertices = ip.face.getVertices();
				Vector3f[] interpolatedSH = SphericalHarmonic.interpolateSphericalHarmonic(vertices, (bounce == 0 ? false : true));
				for (int j = 0; j < coeficientsLimit; ++j) {
					destination[j].x += interpolatedSH[j].x * H * reachedIntensity;
					destination[j].y += interpolatedSH[j].y * H * reachedIntensity;
					destination[j].z += interpolatedSH[j].z * H * reachedIntensity;
				}
			}

		}
		// divide the result by probability / number of samples
		double factor = Math.PI * 2 / samples.length;
		for (int i = 0; i < destination.length; ++i) {
			destination[i].scale((float) factor);
			this.interreflectionFunction[i].set(destination[i]);
		}
	}

	// for now it's average of all faces, need to make barycentric coordinates
	private static Vector3f[] interpolateSphericalHarmonic(Vertex[] vertices, boolean includeInterreflection) {
		final Vector3f[] result = new Vector3f[coeficientsLimit];
		for (int j = 0; j < coeficientsLimit; ++j) {
			result[j] = new Vector3f();
		}
		for (int i = 0; i < vertices.length; ++i) {
			for (int j = 0; j < coeficientsLimit; ++j) {
				Vector3f.add(result[j], vertices[i].sphericalHarmonic.getCoeficient(j, includeInterreflection), result[j]);
			}
		}
		final float factor = 1.0f / vertices.length;
		for (int j = 0; j < coeficientsLimit; ++j) {
			result[j].scale(factor);
		}
		return result;
	}

	// returns true if ready to create SH, false if something is not initialized
	// or initialized not correctly
	// should call this method before any actual SH code
	private static boolean isInitialized() {
		return !(SphericalHarmonic.bandLimit < 0) || (SphericalHarmonic.coeficientsLimit < 0 || SphericalHarmonic.samples == null || SphericalHarmonic.samples.length < SphericalHarmonic.coeficientsLimit);
	}

	// method returns spherical harmonic coeficient for this spherical harmonic
	// if flag includeinterreflection is true, interreflection coeficient is
	// added to the transfer function before returning
	public Vector3f getCoeficient(int id, boolean includeInterreflection) {
		if (includeInterreflection && this.interreflectionFunction != null) {
			return new Vector3f(this.transferFunction[id].x + this.interreflectionFunction[id].x,
					this.transferFunction[id].y + this.interreflectionFunction[id].y,
					this.transferFunction[id].z + this.interreflectionFunction[id].z);
		} else {
			return new Vector3f(this.transferFunction[id]);
		}
	}

	// method returns SH coeficient with interreflection included
	public Vector3f getCoeficient(int id) {
		return this.getCoeficient(id, true);
	}

	public static void updateUsedBands(int i) {
		bandsInUse = (bandsInUse + i + bandLimit + 1) % (bandLimit + 1);
		coeficientsInUse = bandsInUse * bandsInUse;
	}

	public SphericalHarmonic getRotated(Vector3f rotation) {
		final SphericalHarmonic result = new SphericalHarmonic(this);
		result.rotate(rotation);
		return result;
	}

	public void rotate(Vector3f rotation) {
		final int coefsPerChannel = SphericalHarmonic.coeficientsLimit;
		final float R[] = new float[coefsPerChannel];
		final float G[] = new float[coefsPerChannel];
		final float B[] = new float[coefsPerChannel];
		
		int start_index = 1;
		int end_index;
		int coeficientsPerBand;
		float[][] Xpos;
		float[][] Xneg;
		float[][] Ypos;
		float[][] Yneg;
		float[][] Zpos;
		float[][] Zneg;
		float[][] rotationX;
		float[][] rotationY;
		float[][] rotationZ;
		float[][] rotationMatrix;
		for (int i = 1; i < SphericalHarmonic.bandLimit; ++i) {
			coeficientsPerBand = getCoeficientsPerBand(i);
			end_index = start_index + coeficientsPerBand;
			Xpos = HardcodedMatrices.getBandRotationY90PosMatrix(i);
			Xneg = HardcodedMatrices.getBandRotationY90NegMatrix(i);
			Ypos = generateZMatrix((float) (Math.PI / 2), i);
			Yneg = generateZMatrix((float) (-Math.PI / 2), i);
			Zpos = MathUtils.multiplyMatrix(Xpos, Yneg);
			Zneg = MathUtils.multiplyMatrix(Ypos, Xneg);
			rotationX = generateZMatrix(rotation.x, i);
			rotationY = generateZMatrix(rotation.y, i);
			rotationZ = generateZMatrix(rotation.z, i);

			rotationMatrix = MathUtils.multiplyMatrix(Zneg, rotationZ);
			rotationMatrix = MathUtils.multiplyMatrix(rotationMatrix, Zpos);
			rotationMatrix = MathUtils.multiplyMatrix(rotationMatrix, rotationY);
			rotationMatrix = MathUtils.multiplyMatrix(rotationMatrix, Xneg);
			rotationMatrix = MathUtils.multiplyMatrix(rotationMatrix, rotationX);
			rotationMatrix = MathUtils.multiplyMatrix(rotationMatrix, Xpos);

			for (int j = start_index; j < end_index; ++j) {
				for (int k = 0; k < coeficientsPerBand; ++k) {
					R[j] += this.transferFunction[start_index + k].x * rotationMatrix[j - start_index][k];
					G[j] += this.transferFunction[start_index + k].y * rotationMatrix[j - start_index][k];
					B[j] += this.transferFunction[start_index + k].z * rotationMatrix[j - start_index][k];
				}
			}

			start_index = end_index;
		}

		for (int i = 1; i < coefsPerChannel; ++i) {
			this.transferFunction[i].set(R[i], G[i], B[i]);
		}
	}

	private static float[][] generateZMatrix(float angle, int bandNumber) {
		final int coeficientsPerBand = getCoeficientsPerBand(bandNumber);
		final float[][] result = new float[coeficientsPerBand][coeficientsPerBand];
		float argument;
		result[bandNumber][bandNumber] = 1;
		for (int i = 1; i <= bandNumber; ++i) {
			argument = i * angle;
			result[bandNumber - i][bandNumber - i] = (float) Math.cos(argument);
			result[bandNumber + i][bandNumber + i] = result[bandNumber - i][bandNumber - i];
			result[bandNumber - i][bandNumber + i] = (float) Math.sin(argument);
			result[bandNumber + i][bandNumber - i] = -result[bandNumber - i][bandNumber + i];
		}
		return result;
	}

	public static int getCoeficientsPerBand(int bandNumber) {
		return bandNumber * 2 + 1;
	}

	public static void useNumberOfBands(int bandLimit) {
		bandsInUse = bandLimit % (SphericalHarmonic.bandLimit + 1);
		coeficientsInUse = bandsInUse * bandsInUse;
	}

	public void scale(float intensity) {
		for (int i = 0; i < coeficientsLimit; ++i) {
			this.transferFunction[i].scale(intensity);
		}
		if (this.interreflectionFunction != null) {
			this.interreflectionFunction = new Vector3f[coeficientsLimit];
			for (int i = 0; i < coeficientsLimit; ++i) {
				this.interreflectionFunction[i].scale(intensity);
			}
		}

	}

	public void add(SphericalHarmonic right) {
		for (int i = 0; i < coeficientsLimit; ++i) {
			Vector3f.add(this.transferFunction[i], right.getCoeficient(i), this.transferFunction[i]);
		}
	}

}
