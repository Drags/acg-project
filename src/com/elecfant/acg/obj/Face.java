package com.elecfant.acg.obj;

import java.nio.FloatBuffer;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.obj.aabb.AABB;

abstract public class Face {
	protected AABB aabb;

	public Face() {
		this.aabb = new AABB();
	}

	public Face(Vertex vertices[]) {
		return;
	}

	abstract public float[] getPlaneCoeficients();

	public Face[] split(Face splittingFace) {
		return this.split(splittingFace.getPlaneCoeficients());
	}

	abstract public Face[] split(float[] planeCoeficients);

	public void store(FloatBuffer positionBuffer, FloatBuffer uvBuffer, FloatBuffer normalBuffer, FloatBuffer SHBuffer) {
		Vertex vertices[] = this.getVertices();
		for (Vertex vertex : vertices) {
			vertex.position.store(positionBuffer);
			vertex.uv.store(uvBuffer);
			vertex.normal.store(normalBuffer);
			for (int i = 0; i < SphericalHarmonic.coeficientsLimit; ++i) {
				final Vector3f coeficient = vertex.sphericalHarmonic.getCoeficient(i);
				SHBuffer.put(coeficient.x);
				SHBuffer.put(coeficient.y);
				SHBuffer.put(coeficient.z);
			}
		}
	}

	public AABB getAABB() {
		return this.aabb;
	}

	public abstract boolean intersects(Ray ray);

	abstract public Vertex[] getVertices();

	public Vector3f[] getPositions() {
		final Vertex[] vertices = this.getVertices();
		final Vector3f result[] = new Vector3f[vertices.length];
		int i = 0;
		for (Vertex vertex : vertices) {
			result[i++] = vertex.position;
		}
		return result;
	}

	public Vector3f[] getNormals() {
		final Vertex[] vertices = this.getVertices();
		final Vector3f result[] = new Vector3f[vertices.length];
		int i = 0;
		for (Vertex vertex : vertices) {
			result[i++] = vertex.normal;
		}
		return result;
	}

	public Vector3f[] getUVs() {
		final Vertex[] vertices = this.getVertices();
		final Vector3f result[] = new Vector3f[vertices.length];
		int i = 0;
		for (Vertex vertex : vertices) {
			result[i++] = vertex.uv;
		}
		return result;
	}

	abstract public Triangle[] toTriangles();

	abstract public float intersect(Ray r);

	public void calculateInterreflection(OBJModel objModel, int bounce) {
		for (Vertex vertex : this.getVertices()) {
			vertex.sphericalHarmonic.calculateInterreflection(vertex, objModel, bounce);
		}
	}
	
	public void calculateSphericalHarmonics(OBJModel objModel, Material material) {
		for (Vertex vertex : this.getVertices()) {
			vertex.sphericalHarmonic = new SphericalHarmonic(vertex, material, objModel);
		}
	}

	public Vector3f getCenter() {
		final Vector3f positions[] = this.getPositions();
		final Vector3f result = new Vector3f();
		for (Vector3f pos : positions) {
			Vector3f.add(result, pos, result);
		}
		result.scale(1.0f / positions.length);
		return result;
	}

}
