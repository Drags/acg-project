package com.elecfant.acg.obj;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glGetInteger;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_CURRENT_PROGRAM;
import static org.lwjgl.opengl.GL20.glBindAttribLocation;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;

import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.obj.aabb.AABBHierarchy;
import com.elecfant.acg.utils.Utils;

public class Group {
	private String title;
	private List<Face> faces;
	private Material material;

	private int vaoHandle = -1;
	private int positionHandle = -1;
	private int normalHandle = -1;
	private int uvHandle = -1;
	private int shHandle;

	private AABBHierarchy aabb;

	// returns true if material was already assigned and have been overwritten
	// (which means, multiple materials per group)
	public boolean setMaterial(Material newMaterial) {
		if (material == null) {
			material = newMaterial;
			return false;
		} else {
			material = newMaterial;
			return true;
		}
	}

	public Material getMaterial() {
		return this.material;
	}

	public String getTitle() {
		return title;
	}

	public Group(String title) {
		this.title = title;
		faces = new ArrayList<Face>();
		aabb = new AABBHierarchy();
	}

	public void createVAO() {

		// TODO calculate total number of vertices according to face type
		final FloatBuffer positionBuffer = BufferUtils.createFloatBuffer(faces.size() * 9);
		final FloatBuffer uvBuffer = BufferUtils.createFloatBuffer(faces.size() * 9);
		final FloatBuffer normalBuffer = BufferUtils.createFloatBuffer(faces.size() * 9);
		final FloatBuffer SHBuffer = BufferUtils.createFloatBuffer(faces.size() * 3 * SphericalHarmonic.coeficientsLimit * 3);
		for (Face face : faces) {
			face.store(positionBuffer, uvBuffer, normalBuffer, SHBuffer);
		}
		positionBuffer.flip();
		uvBuffer.flip();
		normalBuffer.flip();
		SHBuffer.flip();
		vaoHandle = glGenVertexArrays();
		glBindVertexArray(vaoHandle);

		positionHandle = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, positionHandle);
		glBufferData(GL_ARRAY_BUFFER, positionBuffer, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
		glEnableVertexAttribArray(0);

		uvHandle = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, uvHandle);
		glBufferData(GL_ARRAY_BUFFER, uvBuffer, GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);
		glEnableVertexAttribArray(1);

		normalHandle = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, normalHandle);
		glBufferData(GL_ARRAY_BUFFER, normalBuffer, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
		glEnableVertexAttribArray(2);

		Utils.check_gl_error("before SH buffering");

		shHandle = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, shHandle);
		glBufferData(GL_ARRAY_BUFFER, SHBuffer, GL_STATIC_DRAW);
		// TODO make dependant on bands quantity
		final int coefQuantity = SphericalHarmonic.coeficientsLimit;
		final int floatSize = 4;
		final int channelCount = 3;
		final int stride = coefQuantity * channelCount * floatSize;
		for (int i = 0; i < coefQuantity; ++i) {
			glVertexAttribPointer(3 + i, 3, GL_FLOAT, false,
					stride,
					i * 12 /* 3*4 for each vec3 */);
			glEnableVertexAttribArray(3 + i);
		}

		Utils.check_gl_error("end SH");

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	public void destroy() {

		// Delete textures
		if (this.material.texture_Kd > 0) {
			glDeleteTextures(this.material.texture_Kd);
		}

		// Disable the VBO index from the VAO attributes list
		glBindVertexArray(this.vaoHandle);
		glDisableVertexAttribArray(0); //
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		glDisableVertexAttribArray(3);

		// Delete the VBO
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDeleteBuffers(positionHandle);
		glDeleteBuffers(uvHandle);
		glDeleteBuffers(normalHandle);

		glDeleteBuffers(shHandle);

		// Delete the VAO
		glBindVertexArray(0);
		glDeleteVertexArrays(vaoHandle);

	}

	public void render() {

		if (this.vaoHandle == -1) {
			this.createVAO();
		}
		int current_program = 0;
		current_program = glGetInteger(GL_CURRENT_PROGRAM);

		// TODO replace int current_program by shader parameter
		// TODO create material structure in shaders
		final int diffuseLocation = glGetUniformLocation(current_program, "material_diffuse_color");
		final int specularLocation = glGetUniformLocation(current_program, "material_specular_color");
		final int emissiveLocation = glGetUniformLocation(current_program, "material_emissive_color");
		final int ambientLocation = glGetUniformLocation(current_program, "material_ambient_color");
		final int shininessLocation = glGetUniformLocation(current_program, "material_shininess");
		final int transparencyLocation = glGetUniformLocation(current_program, "material_transparency");

		glUniform3f(diffuseLocation, material.Kd.x, material.Kd.y, material.Kd.z);
		glUniform3f(specularLocation, material.Ks.x, material.Ks.y, material.Ks.z);
		glUniform3f(emissiveLocation, material.Ke.x, material.Ke.y, material.Ke.z);
		glUniform3f(ambientLocation, material.Ka.x, material.Ka.y, material.Ka.z);
		glUniform1f(shininessLocation, material.Ns);
		glUniform1f(transparencyLocation, material.Tr);

		final int diffuseTextureLoaded = glGetUniformLocation(current_program, "material_diffuse_texture");
		if (this.material.texture_Kd > 0) {
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, this.material.texture_Kd);
			glUniform1i(diffuseTextureLoaded, 1);
		} else {
			glUniform1i(diffuseTextureLoaded, 0);
		}

		glBindVertexArray(this.vaoHandle);

		// MathUtils information will be attribute 0
		glBindAttribLocation(current_program, 0, "in_Position");
		// UV information will be attribute 1
		glBindAttribLocation(current_program, 1, "in_UV");
		// Normal information will be attribute 2
		glBindAttribLocation(current_program, 2, "in_Normal");
		// SH buffer
		for (int i = 0; i < SphericalHarmonic.coeficientsLimit; ++i) {
			glBindAttribLocation(current_program, 3 + i, "SH[" + i + "]");
		}

		glDrawArrays(GL_TRIANGLES, 0, faces.size() * 3);
		glBindVertexArray(0);

	}

	public boolean hasMaterial() {
		return this.material != null;
	}

	public void addFace(Face face) {
		this.faces.add(face);
	}

	public boolean intersect(Ray r) {
		return this.aabb.intersect(r);
	}

	public void calculateCoefs(OBJModel objModel) {
		System.out.println("        " + faces.size() + " faces in group");
		int fnr = 0;
		for (Face face : faces) {
			if (++fnr % 100 == 0) {
				System.out.println("            " + (float) fnr / (float) faces.size() + " complete");
			}
			face.calculateSphericalHarmonics(objModel, this.material);
		}
	}

	public void generateAABBHierarchy() {
		this.aabb = new AABBHierarchy(faces, AABBHierarchy.SPLIT_X);

	}

	public void calculateInterreflection(OBJModel objModel, int bounce) {
		System.out.println("        " + faces.size() + " faces in group");
		int fnr = 0;
		for (Face face : faces) {
			if (++fnr % 100 == 0) {
				System.out.println("            " + (float) fnr / (float) faces.size() + " complete");
			}
			face.calculateInterreflection(objModel, bounce);
		}
	}

	public IntersectionPoint getFirstIntersection(Ray r) {
		return this.aabb.getFirstIntersection(r);
	}
}
