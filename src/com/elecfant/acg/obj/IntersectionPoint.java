package com.elecfant.acg.obj;

import org.lwjgl.util.vector.Vector3f;

public class IntersectionPoint {
	public Face face;
	public Vector3f point;
}
