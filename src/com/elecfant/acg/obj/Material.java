package com.elecfant.acg.obj;

import org.lwjgl.util.vector.Vector3f;

public class Material {
	public Vector3f Ka;
	public Vector3f Kd;
	public Vector3f Ks;
	public Vector3f Ke;
	public int texture_Kd;
	public int texture_Ka;
	public float Tr;
	public float Ns;
	public Material() {
		Ka = new Vector3f();
		Kd = new Vector3f();
		Ks = new Vector3f();
		Ke = new Vector3f();
		Tr = 1.0f;
		Ns = 0;
	}
}
