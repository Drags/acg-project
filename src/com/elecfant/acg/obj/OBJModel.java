package com.elecfant.acg.obj;

import static org.lwjgl.opengl.EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT;
import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_LINEAR_MIPMAP_LINEAR;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_UNPACK_ALIGNMENT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glPixelStorei;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameterf;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Adler32;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.game.sh.SphericalHarmonic;
import com.elecfant.acg.obj.aabb.AABB;
import com.elecfant.lib.Map;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class OBJModel {

	public static boolean debug = false;
	private Map<String, Material> materials;
	private List<Group> groups;
	private Group currentGroup;
	private AABB aabb;
	private SphericalHarmonic averageSH;

	public OBJModel(String filename) throws IOException {
		this(new File(filename));
	}

	public OBJModel(File f) throws IOException {
		System.out.println("<OBJ Loading info> File '" + f.getName() + "': Loading...");
		long b = System.nanoTime();
		final List<Vector3f> positions = new ArrayList<Vector3f>();
		final List<Vector3f> normals = new ArrayList<Vector3f>();
		final List<Vector3f> uvs = new ArrayList<Vector3f>();
		final List<Vertex> vertices = new ArrayList<Vertex>();

		aabb = new AABB();

		materials = new Map<String, Material>();
		groups = new ArrayList<Group>();

		String line;
		String type;
		String data;
		float x, y, z;
		int first_space = 0;
		int lineNumber = 0;
		int face_count = 0;
		BufferedReader reader = new BufferedReader(new FileReader(f));
		while ((line = reader.readLine()) != null) {
			++lineNumber;
			line = line.trim();
			first_space = line.indexOf(" ");
			if (line.isEmpty() || line.charAt(0) == '#' || first_space == -1)
				continue;
			type = line.substring(0, first_space).trim().toLowerCase();
			data = line.substring(first_space).trim();
			if (data.isEmpty()) {
				this.warningMessage(f, lineNumber, "Found identifier '" + type + "' but no data");
				continue;
			}
			switch (type) {
			case "mtllib":
				this.loadMaterials(f, data);
				break;
			case "o":
			case "g":
				currentGroup = new Group(data);
				groups.add(currentGroup);
				break;
			case "usemtl":
				if (this.materials.containsKey(data)) {
					if (currentGroup == null || currentGroup.hasMaterial()) {
						currentGroup = new Group("Autogroup_" + data);
						groups.add(currentGroup);
					}
					currentGroup.setMaterial(materials.get(data));
				} else {
					this.warningMessage(f, lineNumber, "Material '" + data + "' was not found in the referenced material libraries");
				}
				break;
			case "v":
				x = Float.valueOf(data.split(" ")[0]);
				y = Float.valueOf(data.split(" ")[1]);
				z = Float.valueOf(data.split(" ")[2]);
				final Vector3f vertexPosition = new Vector3f(x, y, z);
				aabb.updateWith(vertexPosition);
				positions.add(vertexPosition);
				break;
			case "vn":
				x = Float.valueOf(data.split(" ")[0]);
				y = Float.valueOf(data.split(" ")[1]);
				z = Float.valueOf(data.split(" ")[2]);
				normals.add(new Vector3f(x, y, z));
				break;
			case "vt":
				x = Float.valueOf(data.split(" ")[0]);
				if (!data.split(" ")[1].isEmpty()) {

				}
				y = Float.valueOf(data.split(" ")[1]);
				// TODO make everywhere aware of absent parameters
				if (data.split(" ").length == 3) {
					z = Float.valueOf(data.split(" ")[2]);
				} else {
					z = 0;
				}
				uvs.add(new Vector3f(x, y, z));
				break;
			case "f":
				String faceData[] = data.split(" ");
				if (faceData.length < 3) {
					this.warningMessage(f, lineNumber, "Face has to have at least 3 vertices");
					continue;
				} else if (faceData.length > 3) {
					this.warningMessage(f, lineNumber, "Quads and more complex polygons are not supported yet. Use triangles instead");
					continue;
				}
				++face_count;
				if (currentGroup == null) {
					currentGroup = new Group("ungrouped");
					groups.add(currentGroup);
				}
				int positionID = -1,
				normalID = -1,
				uvID = -1;
				final Vertex triangleVertices[] = new Vertex[3];
				for (int i = 0; i < faceData.length; ++i) {
					final Vertex vertex = new Vertex();
					vertices.add(vertex);
					positionID = Integer.valueOf(faceData[i].split("/")[0]) - 1;
					if (!faceData[i].split("/")[1].isEmpty()) {
						uvID = Integer.valueOf(faceData[i].split("/")[1]) - 1;
						vertex.uv = uvs.get(uvID);
					} else {
						uvID = -1;
					}
					normalID = Integer.valueOf(faceData[i].split("/")[2]) - 1;
					vertex.position = positions.get(positionID);
					vertex.normal = normals.get(normalID);
					triangleVertices[i] = vertex;
				}
				currentGroup.addFace(new Triangle(triangleVertices));
				break;
			default:
				warningMessage(f, lineNumber, "Unexpected identifier: " + type);
			}
		}

		reader.close();

		// generating model file hash
		// TODO need faster hashing algorithm
		System.out.println("starting hashing");
		boolean regenerateSHFile = false;
		final File SHFile = new File(f.getCanonicalPath() + ".shc");
		final BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuffer sb = new StringBuffer();
		Adler32 checksum = new Adler32();
		while (br.ready()) {
			sb.append(br.readLine() + "\n");
		}
		br.close();
		String objectString  = new String(sb);
		checksum.update(objectString.getBytes());
		final long objFileChecksum = checksum.getValue();
		warningMessage(f, -1, "Hash value: " + objFileChecksum);
		// end of hash

		// loading precalculated SH file
		if (SHFile.exists()) {
			final BufferedReader shReader = new BufferedReader(new FileReader(SHFile));

			int bandsInFile;
			int arrayCounter = 0;
			int shLine = 0;
			while ((line = shReader.readLine()) != null) {
				++shLine;
				line = line.trim();
				first_space = line.indexOf(" ");
				if (line.isEmpty() || line.charAt(0) == '#' || first_space == -1)
					continue;
				type = line.substring(0, first_space).trim().toLowerCase();
				data = line.substring(first_space).trim();
				if (data.isEmpty()) {
					this.warningMessage(SHFile, shLine, "Found identifier '" + type + "' but no data");
					continue;
				}
				switch (type) {
				case "checksum":
					long inSHChecksum = Long.parseLong(data);
					if (inSHChecksum != objFileChecksum) {
						regenerateSHFile = true;
					}
					break;
				case "max_bands":
					bandsInFile = Integer.parseInt(data);
					if (bandsInFile < SphericalHarmonic.bandLimit) {
						regenerateSHFile = true;
					}
					break;
				case "avg":
					final String avgCoeficients[] = data.split(" ");
					if (avgCoeficients.length < SphericalHarmonic.coeficientsLimit) {
						this.warningMessage(SHFile, shLine, "Spherical Harmonic has not enough coeficients defined in the SHC file.");
						continue;
					}
					final float[] shAverageCoeficientsF = new float[avgCoeficients.length];
					for (int i = 0; i < avgCoeficients.length; ++i) {
						shAverageCoeficientsF[i] = Float.parseFloat(avgCoeficients[i]);

					}
					this.averageSH = new SphericalHarmonic(shAverageCoeficientsF);
					break;
				case "sh":
					final String shCoeficients[] = data.split(" ");
					if (shCoeficients.length < SphericalHarmonic.coeficientsLimit) {
						this.warningMessage(SHFile, shLine, "Spherical Harmonic has not enough coeficients defined in the SHC file.");
						continue;
					}
					final float[] shCoeficientsF = new float[shCoeficients.length];
					for (int i = 0; i < shCoeficients.length; ++i) {
						shCoeficientsF[i] = Float.parseFloat(shCoeficients[i]);

					}
					vertices.get(arrayCounter++).sphericalHarmonic = new SphericalHarmonic(shCoeficientsF);

					break;
				default:
					this.warningMessage(SHFile, shLine, "Unexpected identifier: " + type);
					break;
				}
				if (regenerateSHFile) {
					break;
				}
			}
			shReader.close();
		} else {
			regenerateSHFile = true;
		}

		if (this.averageSH == null) {
			regenerateSHFile = true;
		}

		if (regenerateSHFile) {
			this.warningMessage(f, lineNumber, "SHC file not found. Generating new one");

			System.out.println("generating aabb");
			for (Group group : groups) {
				group.generateAABBHierarchy();
			}

			long start_time = System.nanoTime();
			int gnr = 0;
			for (Group group : groups) {
				System.out.println("   " + (gnr++) + "/" + groups.size() + " group is being processed");
				group.calculateCoefs(this);
			}
			long end_time = System.nanoTime();
			double calculatingCoefsTime = (end_time - start_time) / 1000000.0;
			System.out.println("Caclulating occlusion: " + calculatingCoefsTime + " ms");

			start_time = System.nanoTime();

			System.out.println("    Starting interreflection");
			gnr = 0;
			for (int i = 0; i < SphericalHarmonic.interreflectionBounces; ++i) {
				System.out.println("  " + (i + 1) + " bounce");
				for (Group group : groups) {
					System.out.println("   " + (gnr++) + "/" + groups.size() + " group is being processed");
					group.calculateInterreflection(this, i);
				}
			}

			end_time = System.nanoTime();
			calculatingCoefsTime = (end_time - start_time) / 1000000.0;
			System.out.println("Caclulating interreflection: " + calculatingCoefsTime + " ms");

			// calculating average SH;
			this.averageSH = new SphericalHarmonic(0);
			for (Vertex vertex : vertices) {
				this.averageSH.add(vertex.sphericalHarmonic);
			}
			this.averageSH.scale(1.0f / vertices.size());

			final PrintWriter SHwriter = new PrintWriter(new BufferedWriter(new FileWriter(SHFile, false)));
			SHwriter.println("# Spherical Harmonic Coeficients file. V1.0");
			SHwriter.println("# Accompanies *.OBJ file.");
			SHwriter.println();
			SHwriter.println("# Checksum of corresponding *.OBJ file");
			SHwriter.println("checksum " + String.valueOf(objFileChecksum));
			SHwriter.println();
			SHwriter.println("# Band limit of Spherical Harmonics in this file");
			SHwriter.println("max_bands " + SphericalHarmonic.bandLimit);
			SHwriter.println();
			SHwriter.println("# Average of all SH for inter-object lighting");
			SHwriter.print("avg ");
			final SphericalHarmonic averageSH = this.averageSH;
			Vector3f averageCoeficient;
			for (int i = 0; i < SphericalHarmonic.coeficientsLimit; ++i) {
				averageCoeficient = averageSH.getCoeficient(i);
				SHwriter.print(averageCoeficient.x + " " + averageCoeficient.y + " " + averageCoeficient.z + " ");
			}
			SHwriter.println();
			SHwriter.println();
			SHwriter.println("# Ordered list of spherical harmonics bind to vertices of *.obj file");
			for (Vertex vertex : vertices) {
				SHwriter.print("sh ");
				final SphericalHarmonic sh = vertex.sphericalHarmonic;
				Vector3f coeficient;
				for (int i = 0; i < SphericalHarmonic.coeficientsLimit; ++i) {
					coeficient = sh.getCoeficient(i);
					SHwriter.print(coeficient.x + " " + coeficient.y + " " + coeficient.z + " ");
				}
				SHwriter.println();
			}
			SHwriter.close();

		}
		// end of SH loading

		for (Group group : groups) {
			group.createVAO();
		}

		double loadTime = (System.nanoTime() - b) / 1000000.0;
		System.out.println("<OBJ Loading info> File '" + f.getName() + "': Load complete. Total faces: " + face_count + " Elapsed:" + loadTime + "ms");
		System.out.println(aabb.toString());
	}

	private void warningMessage(File f, int lineNumber, String message) {
		if (debug) {
			System.out.println("<OBJ Loading warning> File '" + f.getName() + "', Line " + lineNumber + ": " + message);
		}
	}

	private void loadMaterials(File OBJFile, String materialFilename) throws IOException {
		final File f = new File(OBJFile.getParentFile(), materialFilename);
		BufferedReader reader = new BufferedReader(new FileReader(f));
		Material currentMaterial = null;
		String line;
		String type;
		String data;
		float r, g, b;
		int first_space = 0;
		int lineNumber = 0;
		while ((line = reader.readLine()) != null) {
			++lineNumber;
			line = line.trim();
			first_space = line.indexOf(" ");
			if (line.isEmpty() || line.charAt(0) == '#' || first_space == -1)
				continue;
			type = line.substring(0, first_space).trim().toLowerCase();
			data = line.substring(first_space).trim();
			if (data.isEmpty()) {
				this.warningMessage(f, lineNumber, "Found identifier '" + type + "' but no data");
				continue;
			}
			switch (type) {
			case "newmtl":
				currentMaterial = new Material();
				materials.put(data, currentMaterial);
				break;
			case "ka":
				r = Float.valueOf(data.split(" ")[0]);
				g = Float.valueOf(data.split(" ")[1]);
				b = Float.valueOf(data.split(" ")[2]);
				currentMaterial.Ka = new Vector3f(r, g, b);
				break;
			case "kd":
				r = Float.valueOf(data.split(" ")[0]);
				g = Float.valueOf(data.split(" ")[1]);
				b = Float.valueOf(data.split(" ")[2]);
				currentMaterial.Kd = new Vector3f(r, g, b);
				break;
			case "ks":
				r = Float.valueOf(data.split(" ")[0]);
				g = Float.valueOf(data.split(" ")[1]);
				b = Float.valueOf(data.split(" ")[2]);
				currentMaterial.Ks = new Vector3f(r, g, b);
				break;
			case "ke":
				r = Float.valueOf(data.split(" ")[0]);
				g = Float.valueOf(data.split(" ")[1]);
				b = Float.valueOf(data.split(" ")[2]);
				currentMaterial.Ke = new Vector3f(r, g, b);
				break;
			case "tr":
				currentMaterial.Tr = 1 - Float.valueOf(data);
				break;
			case "d":
				currentMaterial.Tr = Float.valueOf(data);
				break;
			case "ns":
				currentMaterial.Ns = Float.valueOf(data);
				break;
			case "map_kd":
				final int textureKdHandle = loadTextureFile(f, data);
				currentMaterial.texture_Kd = textureKdHandle;
				break;
			default:
				warningMessage(f, lineNumber, "Unexpected identifier: " + type);
			}
		}
		reader.close();
	}

	private int loadTextureFile(File f, String data) throws IOException {
		File textureFile = new File(f.getParentFile(), data);
		ByteBuffer buf = null;
		int tWidth = 0;
		int tHeight = 0;

		// Open the PNG file as an InputStream
		InputStream in = new FileInputStream(textureFile);
		// Link the PNG decoder to this stream
		PNGDecoder decoder = new PNGDecoder(in);

		// Get the width and height of the texture
		tWidth = decoder.getWidth();
		tHeight = decoder.getHeight();

		// Decode the PNG file in a ByteBuffer
		buf = ByteBuffer.allocateDirect(
				4 * decoder.getWidth() * decoder.getHeight());
		decoder.decode(buf, decoder.getWidth() * 4, Format.RGBA);
		buf.flip();
		in.close();

		// Create a new texture object in memory and bind it
		final int texId = glGenTextures();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texId);

		// All RGB bytes are aligned to each other and each component is 1 byte
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		// Upload the texture data and generate mip maps (for scaling)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tWidth, tHeight, 0,
				GL_RGBA, GL_UNSIGNED_BYTE, buf);
		glGenerateMipmap(GL_TEXTURE_2D);

		// Setup the ST coordinate system
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);
		// Setup what to do when the texture has to be scaled
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
				GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				GL_LINEAR_MIPMAP_LINEAR);

		return texId;

	}

	public void destroy() {
		for (Group group : groups) {
			group.destroy();
		}
	}

	public void render() {

		for (Group group : groups) {
			group.render();
		}
	}

	public boolean intersect(Ray r) {
		if (this.aabb.intersect(r)) {
			for (Group group : groups) {
				if (group.intersect(r)) {
					return true;
				}
			}
		}
		return false;
	}

	public IntersectionPoint getFirstIntersection(Ray r) {
		IntersectionPoint result = null;
		if (this.aabb.intersect(r)) {
			for (Group group : groups) {
				// TODO check closest intersection, too
				result = group.getFirstIntersection(r);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	public AABB getAABB() {
		return this.aabb;
	}
}
