package com.elecfant.acg.obj;

import org.lwjgl.util.vector.Vector3f;

public class Ray {
	public Vector3f origin;
	public Vector3f direction;
	public float maximumLength;

	public Ray() {
		this.origin = new Vector3f();
		this.direction = new Vector3f();
	}

	public Ray(Vector3f origin, Vector3f direction) {
		this.origin = new Vector3f(origin);
		this.direction = new Vector3f(direction);
		this.direction.normalise();
	}

	public Vector3f getVector3f(float t) {
		final Vector3f result = new Vector3f(origin);
		final Vector3f distance = new Vector3f(direction);
		distance.scale(t);
		Vector3f.add(result, distance, result);
		return result;
	}

}
