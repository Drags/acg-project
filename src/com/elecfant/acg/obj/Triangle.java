package com.elecfant.acg.obj;

import org.lwjgl.util.vector.Vector3f;

public class Triangle extends Face {

	protected Vertex vertices[];

	public Triangle() {
		vertices = new Vertex[3];
		for (int i = 0; i < 3; ++i) {
			vertices[i] = new Vertex();
		}
	}

	public Triangle(Vertex[] vertices) {
		super();
		this.vertices = new Vertex[3];
		for (int i = 0; i < 3; ++i) {
			this.vertices[i] = vertices[i];
			this.aabb.updateWith(vertices[i].position);
		}
	}

	@Override
	public float[] getPlaneCoeficients() {
		// Get Shortened Names For The Vertices Of The Face
		final Vector3f v1 = vertices[0].position;
		final Vector3f v2 = vertices[1].position;
		final Vector3f v3 = vertices[2].position;

		final float result[] = new float[4];
		result[0] = v1.y * (v2.z - v3.z) + v2.y * (v3.z - v1.z) + v3.y * (v1.z - v2.z);
		result[1] = v1.z * (v2.x - v3.x) + v2.z * (v3.x - v1.x) + v3.z * (v1.x - v2.x);
		result[2] = v1.x * (v2.y - v3.y) + v2.x * (v3.y - v1.y) + v3.x * (v1.y - v2.y);
		result[3] = -(v1.x * (v2.y * v3.z - v3.y * v2.z) +
				v2.x * (v3.y * v1.z - v1.y * v3.z) +
				v3.x * (v1.y * v2.z - v2.y * v1.z));
		return result;
	}

	@Override
	public boolean intersects(Ray ray) {
		if (!this.aabb.intersect(ray)) {
			return false;
		}
		final Vector3f e1 = new Vector3f();
		final Vector3f e2 = new Vector3f();
		final Vector3f s = new Vector3f();
		Vector3f.sub(this.vertices[1].position, this.vertices[0].position, e1);
		Vector3f.sub(this.vertices[2].position, this.vertices[0].position, e2);
		Vector3f.sub(ray.origin, this.vertices[0].position, s);

		final Vector3f de2 = new Vector3f();
		Vector3f.cross(ray.direction, e2, de2);
		final float f = 1 / Vector3f.dot(de2, e1);
		final float u = f * Vector3f.dot(de2, s);
		if (u < 0 || u > 1) {
			return false;
		}

		final Vector3f se1 = new Vector3f();
		Vector3f.cross(s, e1, se1);

		final float v = f * Vector3f.dot(se1, ray.direction);

		if (v < 0 || v > 1) {
			return false;
		}
		if (u + v > 1) {
			return false;
		}

		final float t = f * Vector3f.dot(se1, e2);
		if (t < 0) {
			return false;
		}
		return true;
	}

	@Override
	public Vertex[] getVertices() {
		return this.vertices;
	}

	@Override
	public Face[] split(float[] planeCoeficients) {
		// TODO still todo
		final Face result[] = new Face[3];
		// if (planeCoeficients == null || planeCoeficients.length != 4) throw
		// new IllegalArgumentException();
		// final Vector3f n = new Vector3f(planeCoeficients[0],
		// planeCoeficients[1], planeCoeficients[2]);
		// final float d = planeCoeficients[3];
		//
		// final float planeValues[] = new float[3];
		// for (int i = 0; i < 3; ++i) {
		//
		// }
		//
		// final Vector3f dir0 = Vector3f.sub(this.vertices[1].position,
		// this.vertices[0].position, null);
		// final Ray r0 = new Ray(this.vertices[0].position, dir0);
		// r0.maximumLength = dir0.length();
		// final Vector3f dir1 = Vector3f.sub(this.vertices[2].position,
		// this.vertices[1].position, null);
		// final Ray r1 = new Ray(this.vertices[1].position, dir1);
		// r1.maximumLength = dir1.length();
		// final Vector3f dir2 = Vector3f.sub(this.vertices[0].position,
		// this.vertices[2].position, null);
		// final Ray r2 = new Ray(this.vertices[2].position, dir2);
		// r2.maximumLength = dir2.length();
		return result;
	}

	@Override
	public Triangle[] toTriangles() {
		return new Triangle[] { this };
	}

	@Override
	public float intersect(Ray r) {
		// returns intersection point (in terms of ray parameter)
		if (!this.aabb.intersect(r)) {
			return Float.NEGATIVE_INFINITY;
		}
		final Vector3f e1 = new Vector3f();
		final Vector3f e2 = new Vector3f();
		final Vector3f s = new Vector3f();
		Vector3f.sub(this.vertices[1].position, this.vertices[0].position, e1);
		Vector3f.sub(this.vertices[2].position, this.vertices[0].position, e2);
		Vector3f.sub(r.origin, this.vertices[0].position, s);

		final Vector3f de2 = new Vector3f();
		Vector3f.cross(r.direction, e2, de2);
		final float f = 1 / Vector3f.dot(de2, e1);
		final float u = f * Vector3f.dot(de2, s);
		if (u < 0 || u > 1) {
			return Float.NEGATIVE_INFINITY;
		}

		final Vector3f se1 = new Vector3f();
		Vector3f.cross(s, e1, se1);

		final float v = f * Vector3f.dot(se1, r.direction);

		if (v < 0 || v > 1) {
			return Float.NEGATIVE_INFINITY;
		}
		if (u + v > 1) {
			return Float.NEGATIVE_INFINITY;
		}

		final float t = f * Vector3f.dot(se1, e2);
		return t;
	}

}
