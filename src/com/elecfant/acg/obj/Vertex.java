package com.elecfant.acg.obj;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.game.sh.SphericalHarmonic;

public class Vertex {
	public Vector3f position;
	public Vector3f normal;
	public Vector3f uv;
	
	public SphericalHarmonic sphericalHarmonic;
	public boolean sampleOccluded[];

	public Vertex() {
		this.position = new Vector3f();
		this.normal = new Vector3f();
		this.uv = new Vector3f();
	}

	public Vertex(Vertex original) {
		this.position = new Vector3f(original.position);
		this.normal = new Vector3f(original.normal);
		this.uv = new Vector3f(original.uv);
		this.sphericalHarmonic = new SphericalHarmonic(original.sphericalHarmonic);
	}
}
