package com.elecfant.acg.obj.aabb;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.obj.Face;
import com.elecfant.acg.obj.Ray;

public class AABB {
	public Vector3f min;
	public Vector3f max;

	public AABB() {
		min = new Vector3f(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
		max = new Vector3f(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
	}

	public AABB(Vector3f min, Vector3f max) {
		this.min = new Vector3f(min);
		this.max = new Vector3f(max);
	}

	@Override
	public String toString() {
		return super.toString() + "\nMin: " + min.x + "x" + min.y + "x" + min.z + " Max: " + max.x + "x" + max.y + "x" + max.z;
	}
	
	public void updateWith(Vector3f newVertex) {
		if (newVertex.x < min.x)
			min.x = newVertex.x;
		if (newVertex.y < min.y)
			min.y = newVertex.y;
		if (newVertex.z < min.z)
			min.z = newVertex.z;
		if (newVertex.x > max.x)
			max.x = newVertex.x;
		if (newVertex.y > max.y)
			max.y = newVertex.y;
		if (newVertex.z > max.z)
			max.z = newVertex.z;
	}



	public void updateWith(AABB aabb) {
		this.updateWith(aabb.min);
		this.updateWith(aabb.max);
	}
	
	public void updateWith(Face face) {
		for (Vector3f vertexPosition : face.getPositions()) {
			this.updateWith(vertexPosition);
		}
	}
	
	public Vector3f getCenter(){
		final Vector3f center = new Vector3f();
		Vector3f.add(this.min, this.max, center);
		center.scale(0.5f);
		return center;
	}
	
	public float getDistance(AABB other) {
		final Vector3f distance = new Vector3f();
		Vector3f.sub(this.getCenter(), other.getCenter(), distance);
		return distance.length();
	}

	public boolean intersect(Ray r) {
		// TODO create another method with actual return
		// final float t;
		// r.dir is unit direction vector of ray
		final Vector3f frd = new Vector3f();
		frd.x = 1.0f / r.direction.x;
		frd.y = 1.0f / r.direction.y;
		frd.z = 1.0f / r.direction.z;
		// lb is the corner of AABB with minimal coordinates - left bottom, rt
		// is maximal corner
		// r.org is origin of ray
		final float t1 = (min.x - r.origin.x) * frd.x;
		final float t2 = (max.x - r.origin.x) * frd.x;
		final float t3 = (min.y - r.origin.y) * frd.y;
		final float t4 = (max.y - r.origin.y) * frd.y;
		final float t5 = (min.z - r.origin.z) * frd.z;
		final float t6 = (max.z - r.origin.z) * frd.z;

		final float tmin = Math.max(Math.max(Math.min(t1, t2), Math.min(t3, t4)), Math.min(t5, t6));
		final float tmax = Math.min(Math.min(Math.max(t1, t2), Math.max(t3, t4)), Math.max(t5, t6));

		// if tmax < 0, ray (line) is intersecting AABB, but whole AABB is
		// behing us
		if (tmax < 0)
		{
			// t = tmax;
			return false;
		}

		// if tmin > tmax, ray doesn't intersect AABB
		if (tmin > tmax)
		{
			// t = tmax;
			return false;
		}

		// t = tmin;
		return true;
	}
	
	public boolean intersect(AABB other) {
            if (this.max.x < other.min.x || 
                this.max.y < other.min.y ||
                this.max.z > other.min.z ||
                this.min.x > other.max.x || 
                this.min.y > other.max.y ||
                this.min.z > other.max.z) {
                return false;
            }
    return true;
	}

	public Vector3f[] getAllVertices() {
		final Vector3f[] result = new Vector3f[8];
		result[0] = new Vector3f(min.x, min.y, min.z);
		result[1] = new Vector3f(min.x, min.y, max.z);
		result[2] = new Vector3f(min.x, max.y, min.z);
		result[3] = new Vector3f(min.x, max.y, max.z);
		result[4] = new Vector3f(max.x, min.y, min.z);
		result[5] = new Vector3f(max.x, min.y, max.z);
		result[6] = new Vector3f(max.x, max.y, min.z);
		result[7] = new Vector3f(max.x, max.y, max.z);
		return result;
	}

	public float maxDistance() {
		return Vector3f.sub(this.min, this.max, null).length();
	}

	public boolean hasInside(Vector3f point) {
		return this.intersect(new AABB(new Vector3f(point), new Vector3f(point)));
	}

}
