package com.elecfant.acg.obj.aabb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;

import com.elecfant.acg.obj.Face;
import com.elecfant.acg.obj.IntersectionPoint;
import com.elecfant.acg.obj.Ray;

public class AABBHierarchy extends AABB {

	public final static short SPLIT_X = 0;
	public final static short SPLIT_Y = 1;
	public final static short SPLIT_Z = 2;
	private AABBHierarchy positive;
	private AABBHierarchy negative;
	private List<Face> faces;
	private int facesPerChildren;
	private short splitDirection = SPLIT_X;

	public AABBHierarchy() {
		faces = new ArrayList<Face>();
		this.facesPerChildren = 2;
	}

	public boolean intersect(Ray r) {
		if (!super.intersect(r)) {
			return false;
		}
		if (positive != null && negative != null) {
			return positive.intersect(r) || negative.intersect(r);
		} else {
			for (Face face : faces) {
				if (face.intersects(r)) {
					return true;
				}
			}
		}
		return false;
	}

	public AABBHierarchy(Collection<Face> inputFaces, short splitDirection) {
		this();
		this.splitDirection = splitDirection;
		for (Face face : inputFaces) {
			this.updateWith(face);
		}

		if (inputFaces.size() > facesPerChildren) {
			final AABB positiveAABB = new AABB();
			final AABB negativeAABB = new AABB();
			final Vector3f center = this.getCenter();
			final List<Face> positiveFaces = new ArrayList<Face>();
			final List<Face> negativeFaces = new ArrayList<Face>();
			positiveAABB.min = new Vector3f(this.min);
			positiveAABB.max = new Vector3f(this.max);
			negativeAABB.min = new Vector3f(this.min);
			negativeAABB.max = new Vector3f(this.max);
			switch (this.splitDirection) {
			case SPLIT_X:
				positiveAABB.min.x = center.x;
				negativeAABB.max.x = center.x;
				break;
			case SPLIT_Y:
				positiveAABB.min.y = center.y;
				negativeAABB.max.y = center.y;
				break;
			case SPLIT_Z:
				positiveAABB.min.z = center.z;
				negativeAABB.max.z = center.z;
				break;
			default:
				positiveAABB.min.z = center.z;
				negativeAABB.max.z = center.z;
				break;
			}

			for (Face face : inputFaces) {
				final AABB faceAABB = face.getAABB();
				if (negativeAABB.getDistance(faceAABB) < positiveAABB.getDistance(faceAABB)) {
					negativeFaces.add(face);
				} else {
					positiveFaces.add(face);
				}
			}
			final short nextSplit = (short) ((this.splitDirection + 1) % 3);
			if (positiveFaces.size() == 0) {
				for (Face negativeFace : negativeFaces) {
					this.faces.add(negativeFace);
				}
			} else if (negativeFaces.size() == 0) {
				for (Face positiveFace : positiveFaces) {
					this.faces.add(positiveFace);
				}
			} else {
				this.positive = new AABBHierarchy(positiveFaces, nextSplit);
				this.negative = new AABBHierarchy(negativeFaces, nextSplit);
			}

		} else {
			for (Face face : inputFaces) {
				this.faces.add(face);
			}
		}
	}

	public AABBHierarchy(List<Face> positiveFaces, short s, int size) {
		// TODO Auto-generated constructor stub
	}

	public void recalculateAABB() {
		// clear the current AABB
		this.min = new Vector3f(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
		this.max = new Vector3f(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);

		// loop through all faces and recalculate AABB
		// TODO make it loop through all childs and etc
		for (Face face : faces) {
			this.updateWith(face);
		}
	}

	public IntersectionPoint getFirstIntersection(Ray r) {
		IntersectionPoint result = null;
		if (!super.intersect(r)) {
			return null;
		}
		if (positive != null && negative != null) {
			result = positive.getFirstIntersection(r);
			if (result == null) {
				result = negative.getFirstIntersection(r);
			}
			return result;
		} else {
			float min_t = Float.POSITIVE_INFINITY;
			for (Face face : faces) {
				float t = face.intersect(r);
				if (t > 0 && t < min_t) {
					if (result == null) {
						result = new IntersectionPoint();
					}
					min_t = t;
					result.face = face;
					result.point = r.getVector3f(t);
				}
			}
			return result;
		}
	}

}
