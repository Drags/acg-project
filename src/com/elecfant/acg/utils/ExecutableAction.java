package com.elecfant.acg.utils;

public interface ExecutableAction {
	public abstract Object[] Execute(Object arguments[]);
}
