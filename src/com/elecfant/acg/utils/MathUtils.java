package com.elecfant.acg.utils;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import java.lang.Math;

public class MathUtils {

	
	/**
	 * @param radius
	 * @param theta
	 * @param phi
	 * @return
	 */
	public static Vector3f sphericalToCarthesian(float radius, float theta, float phi) {
		Vector3f result = new Vector3f();
		result.x = (float) (radius * Math.sin(theta) * Math.sin(phi));
		result.y = (float) (radius * Math.cos(phi));
		result.z = (float) (radius * Math.cos(theta) * Math.sin(phi));
		return result;
	}
	public static Vector3f carthesianToSpherical(float x, float y, float z) {
		return carthesianToSpherical(new Vector3f(x,y,z));
	}

	public static Vector3f carthesianToSpherical(Vector3f coords) {
		final Vector3f result = new Vector3f();
		final float r = coords.length();
		final float phi = (float) Math.atan2(coords.x,coords.z);
		final float theta = (float) Math.acos(coords.y/r);
		result.set(r, theta, phi);
		return result;
	}
	public static Matrix4f lookFromAt(Vector3f from, Vector3f at, Vector3f up) {
		final Vector3f zaxis = Vector3f.sub(from, at, null).normalise(null);
		final Vector3f xaxis = Vector3f.cross(up, zaxis, null).normalise(null);
		final Vector3f yaxis = Vector3f.cross(zaxis, xaxis, null).normalise(null);
		Matrix4f left = new Matrix4f();
		left.m00 = xaxis.x;
		left.m01 = yaxis.x;
		left.m02 = zaxis.x;
		left.m03 = 0;
		left.m10 = xaxis.y;
		left.m11 = yaxis.y;
		left.m12 = zaxis.y;
		left.m13 = 0;
		left.m20 = xaxis.z;
		left.m21 = yaxis.z;
		left.m22 = zaxis.z;
		left.m23 = 0;
		left.m30 = 0;
		left.m31 = 0;
		left.m32 = 0;
		left.m33 = 1;
		return Matrix4f.mul(left, new Matrix4f().translate(from.negate(null)), null);
	}

	public static float coTangent(float angle) {
		return (float) (1 / Math.tan(angle));
	}

	public static float degreesToRadians(float angle) {
		return (float) (angle * (Math.PI / 180.0));
	}

	public static float random(float from, float to) {
		return (float) (Math.random() * (to - from) + from);
	}

	public static Matrix4f createRotationMatrix(Vector3f rotations) {
		final Vector3f xaxis = new Vector3f(1,0,0);
		final Matrix4f rotationMat = Matrix4f.rotate(rotations.x, xaxis, new Matrix4f(), null);
		
		final Vector4f temp_yaxis = new Vector4f(0,1,0,0);
		Matrix4f.transform(Matrix4f.invert(rotationMat, null), temp_yaxis, temp_yaxis);
		final Vector3f yaxis = new Vector3f(temp_yaxis.x, temp_yaxis.y, temp_yaxis.z);
		Matrix4f.rotate(rotations.y, yaxis, rotationMat, rotationMat);
		
		final Vector4f temp_zaxis = new Vector4f(0,0,1,0);
		Matrix4f.transform(Matrix4f.invert(rotationMat, null), temp_zaxis, temp_zaxis);
		final Vector3f zaxis = new Vector3f(temp_zaxis.x, temp_zaxis.y, temp_zaxis.z);
		Matrix4f.rotate(rotations.z, zaxis, rotationMat, rotationMat);
		
		return rotationMat;
	}
	
	// multiplies 2-dimensional float array as it was matrices. returns null if row/column quantity is incorrect
	// otherwise returns resulting matrix
	public static float[][] multiplyMatrix(float[][] left, float[][] right) {
		if (left[0].length != right.length) return null; 
		final float[][] result = new float[left.length][right[0].length];
		for (int i = 0; i < left.length; ++i) {
			for (int j = 0; j < right[0].length; ++j) {
				result[i][j] = 0;
				for (int k = 0; k < left[0].length; ++k) {
					result[i][j] += left[i][k] * right[k][j];
				}
			}
		}
		return result;
	}

	public static float[][] generateIdentityMatrix(int m, int n) {
		final float[][] result = new float[m][n];
		for (int i = 0; i < m && i < n; ++i) {
			result[i][i] = 1;
		}
		return result;
	}

}
