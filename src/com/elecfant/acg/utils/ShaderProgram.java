package com.elecfant.acg.utils;

import static com.elecfant.acg.utils.Utils.check_gl_error;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.glGetInteger;
import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class ShaderProgram {

	private int vertexShaderHandle = 0;
	private int fragmentShaderHandle = 0;
	private int shaderProgramHandle = 0;

	private int compilationStatus = 1;

	private int previousProgram = 0;

	public ShaderProgram(String vertex, String fragment) {
		vertexShaderHandle = loadShader(vertex, GL_VERTEX_SHADER);
		check_gl_error("loading vertex shaders");
		// Load the fragment shader
		fragmentShaderHandle = loadShader(fragment, GL_FRAGMENT_SHADER);
		check_gl_error("loading fragment shaders");
		shaderProgramHandle = glCreateProgram();
		glAttachShader(shaderProgramHandle, vertexShaderHandle);
		glAttachShader(shaderProgramHandle, fragmentShaderHandle);
		glLinkProgram(shaderProgramHandle);

		glValidateProgram(shaderProgramHandle);

		check_gl_error("loading shaders");
	}

	private int loadShader(String filename, int type) {
		StringBuilder shaderSource = new StringBuilder();
		int shaderID = 0;

		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = reader.readLine()) != null) {
				shaderSource.append(line).append("\n");
			}
			reader.close();
		} catch (IOException e) {
			System.err.println("Could not read file.");
			e.printStackTrace();
			System.exit(-1);
		}

		shaderID = glCreateShader(type);
		glShaderSource(shaderID, shaderSource);
		glCompileShader(shaderID);

		compilationStatus *= glGetShaderi(shaderID, GL_COMPILE_STATUS);
		if (compilationStatus == GL_FALSE) {
			System.out.println("Shader '" + filename + "' failed to compile");
		}
		return shaderID;
	}

	public boolean isValid() {
		return compilationStatus * shaderProgramHandle != 0;
	}

	public void destroy() {
		// safety check before disposal
		if (glGetInteger(GL_CURRENT_PROGRAM) == shaderProgramHandle) {
			glUseProgram(0);
		}

		glDetachShader(shaderProgramHandle, vertexShaderHandle);
		glDetachShader(shaderProgramHandle, fragmentShaderHandle);
		glDeleteShader(vertexShaderHandle);
		glDeleteShader(fragmentShaderHandle);
		glDeleteProgram(shaderProgramHandle);
	}

	public void Use() {
		previousProgram = glGetInteger(GL_CURRENT_PROGRAM);
		glUseProgram(this.shaderProgramHandle);
	}

	public void setUniform(String name, Vector3f value) {
		glUniform3f(glGetUniformLocation(this.shaderProgramHandle, name), value.x, value.y, value.z);
	}

	public void setUniform(String name, int value) {
		glUniform1i(glGetUniformLocation(this.shaderProgramHandle, name), value);
	}

	public void setUniform(String name, float value) {
		glUniform1f(glGetUniformLocation(this.shaderProgramHandle, name), value);
	}

	public void switchBack() {
		glUseProgram(previousProgram);
	}

	public void setUniform(String name, Matrix4f matrix) {
		final int matrixHandle = glGetUniformLocation(this.shaderProgramHandle, name);
		final FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
		matrix.store(matrixBuffer);
		matrixBuffer.flip();
		glUniformMatrix4(matrixHandle, false, matrixBuffer);
	}

	public int getProgramID() {
		return this.shaderProgramHandle;
	}

	public void setUniform(String name, Vector2f value) {
		glUniform2f(glGetUniformLocation(this.shaderProgramHandle, name), value.x, value.y);
	}

}
