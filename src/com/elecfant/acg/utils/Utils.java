package com.elecfant.acg.utils;

import static org.lwjgl.opengl.GL11.GL_NO_ERROR;
import static org.lwjgl.opengl.GL11.glGetError;

public class Utils {
	public static void check_gl_error(String string) {
		int error = glGetError();
		if (error != GL_NO_ERROR) {
			System.out.println(string + ": " + error);
			System.out.println("\t" + org.lwjgl.opengl.Util.translateGLErrorString(error));
		}
	}
}
